<?php

namespace Tests\App\Services\Api;

use PHPUnit\Framework\TestCase;
use App\Services\Api\CurrencyService;

Class CurrencyServiceTest extends TestCase
{

    public function testGetAvailableCurrencies() {

        $availableCurrencies = CurrencyService::getAvailableCurrencies();
        $expectedCurrencies = ['USD', 'EUR'];

        $this->assertSame($expectedCurrencies, $availableCurrencies);
    }

    public function testGetNotAvailableCurrencies() {

        $availableCurrencies = CurrencyService::getAvailableCurrencies();
        $expectedCurrencies = ['BRL'];

        $this->assertNotSame($expectedCurrencies, $availableCurrencies);
    }

    public function testIsAvailableToConvert() {

        $currency = 'EUR';
        $this->assertTrue(CurrencyService::isAvailableToConvert($currency));

    }

    public function testIsNotAvailableToConvert() {

        $currency = 'BRL';
        $this->assertFalse(CurrencyService::isAvailableToConvert($currency));

    }

}
