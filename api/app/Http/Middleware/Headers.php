<?php

namespace App\Http\Middleware;

use Closure;

class Headers
{    
    public function handle($request, Closure $next)
    {        
        $localization = $request->header('X-Localization');

        if($localization){
            \App::setLocale($localization);
        }        

        return $next($request);
    }
    
}
