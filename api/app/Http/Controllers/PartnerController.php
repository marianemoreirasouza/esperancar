<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Partner;
use App\Services\Partner\PartnerService;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Traits\Storage;

class PartnerController extends ApiController
{
    use HttpResponse, Storage;

    /**
     * Constructor function
     */
    public function __construct(PartnerService $service, Partner $model)
    {
        parent::__construct($service, $model);
    }

    public function index()
    {
        return $this->showAll($this->service->index());
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $partner = $this->service->store($request);
            $partner->save();

            if ($request->has('img_partner')) {
                $file = $this->putFile('partners', $partner->arquivo(), $request->img_partner);
                DB::table('partners')->where('id', $partner->id)
                    ->update(
                        ['file_id' => $file->id]
                    );
            }

            DB::commit();

            return $partner;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $partner = $this->service->update($request, $id);
            $partner->save();

            if ($request->has('img_partner')) {
                $file = $this->putFile('partners', $partner->arquivo(), $request->img_partner);
                DB::table('partners')->where('id', $partner->id)
                    ->update(
                        ['file_id' => $file->id]
                    );
            }

            DB::commit();

            return $partner;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }
}
