<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inform;
use App\Models\User;
use App\Services\Inform\InformService;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Traits\Storage;
use Illuminate\Support\Facades\Log;
use App\Services\Alert\AlertService;

class InformController extends ApiController
{
    use HttpResponse, Storage;

    /**
     * Constructor function
     */
    public function __construct(InformService $service, Inform $model)
    {
        parent::__construct($service, $model);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $inform = $this->service->store($request);
            $inform->save();

            if ($request->has('img_info')) {
                $file = $this->putFile('informs', $inform->arquivo(), $request->img_info);
                DB::table('informs')->where('id', $inform->id)
                    ->update(
                        ['file_id' => $file->id]
                    );
            }

            // enviar alerta doadores
            $title = '';

            if ($inform->type == 'info') {
                $title = 'Informação';
            } else if ($inform->type == 'thank') {
                $title = 'Agradecimento';
            } else {
                $title = 'Importante';
            }

            $alertService = new AlertService();
            $users = User::where('alerts', 1)->whereHas('profile', function ($q) {
                $q->where('type', 'Doador');
            })->get();

            foreach ($users as $key => $user) {
                $alertService->createNotification([
                    'user_id' => $user->id,
                    'inform_id' => $inform->id,
                    'is_read' => 0,
                    'title' => $title,
                    'text' => $inform->text
                ]);
            }

            DB::commit();

            return $inform;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }
}
