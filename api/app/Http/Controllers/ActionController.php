<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Action;
use App\Models\User;
use App\Traits\AuthorizePermission;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Services\Action\ActionService;
use Illuminate\Support\Facades\Log;
use App\Services\Alert\AlertService;

class ActionController extends ApiController
{
    use HttpResponse;

    /**
     * Constructor function
     */
    public function __construct(ActionService $service, Action $model) {
        parent::__construct($service, $model);
    }

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            $action = $this->service->store($request);

            $action->save();

            // enviar invite doadores 
            $alertService = new AlertService();
            $doadores = explode(",", $request->doadores);
            $users = User::whereIn('id', $doadores)->where('alerts', 1)->whereHas('profile', function ($q) {
                $q->where('type', 'Doador');
            })->get();

            foreach ($users as $key => $user) {
                $alertService->createNotification([
                    'user_id' => $user->id,
                    'inform_id' => null,
                    'is_read' => 0,
                    'title' => 'Uma nova ação começou!',
                    'text' => 'Venha fazer sua doação para a nova ação desse mês'
                ]);
            }

            DB::commit();

            return $action;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    public function getWithUsers(Request $request)
    {
        $action = Action::where('id', $request->id)->with('doacao.doador');

        return response()->json($action->first());
    }
}