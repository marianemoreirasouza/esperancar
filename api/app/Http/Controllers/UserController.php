<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use App\Traits\AuthorizePermission;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgottenPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Log;

class UserController extends ApiController
{
    use AuthorizePermission, HttpResponse;

    /**
     * Constructor function
     */
    public function __construct(UserService $service, User $model) {
        parent::__construct($service, $model);
    }

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            $user = $this->service->store($request);

            if(isset($request->password)){
                $user->password = bcrypt($request->password);
            }else{
                $user->password = Str::random(191);
            }

            $user->save();

            DB::commit();
            // Mail::to($user)->send(new RegisterPassword($user));

            DB::table('profiles')->insert(
                ['user_id' => $user->id, 'type' => 'Doador']
            );

            return $user;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    public function update(Request $request, $id) {
        DB::beginTransaction();
        try {
            $user = $this->service->update($request, $id);
            $user->save();

            DB::commit();

            return $user;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json(null, 204);
    }

    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        $user->restore();
        return response()->json(null, 204);
    }

    public function trashed(Request $request)
    {
        $users = $this->service->index()->onlyTrashed();

        if ($request->name) {
            $users->where(function($query) use($request){
                $query->where('name', 'like', $request->name)
                    ->orWhere('email', 'like', $request->name);
            });
        }

        return $this->showResponse($users->paginate());
    }

    public function getWithProfile(Request $request)
    {
        $users = User::with('profile');

        if (isset($request->search)) {
            $users->where('name', 'like', "%{$request->search}%");
        }

        if (isset($request->profile_type))
            $users->whereHas('profile', function ($q) use ($request) {
                $q->where('type', $request->profile_type);
            });


        if (isset($request->order) && isset($request->field)) {
            if ($request->order == 'desc') {
                $users = $users->sortByDesc($request->field);
            } else {
                $users = $users->sortBy($request->field);
            }
        }

        return response()->json($users->paginate($request->per_page));
    }

    public function verifyTokenRegisterPassword(Request $request)
    {
        $data = (object) $request->only(['token', 'id']);

        if (strlen($data->token) != 191) return response()->json(['status' => 'not found']);

        if (User::where('id', $data->id)->where('password', $data->token)->count()) return response()->json(['status' => 'ok']);

        return response()->json(['status' => 'not found']);
    }

    public function registerPassword(Request $request)
    {
        $data = (object) $request->only(['id', 'token', 'password']);

        $user = User::find($data->id);

        if ($user->where('password', $data->token)->count()) {
            $user->password = bcrypt($data->password);
            $user->save();
        }

        return response()->json(['status' => 'ok']);
    }

    public function requestForgottenPassword(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->first();

            if (!$user) return response()->json(['status' => 'not found']);

            if (!DB::table('password_resets')->where('email', $user->email)->count()) {
                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => Str::random(191)
                ]);
            }

            $password_reset = DB::table('password_resets')->where('email', $user->email)->first();

            Mail::to($user)->send(new ForgottenPassword($password_reset));

            return response()->json(['status' => 'ok']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }

    public function verifyTokenResetPassword(Request $request)
    {
        $data = (object) $request->only(['token', 'email']);

        if (DB::table('password_resets')->where('email', $data->email)->where('token', $data->token)->count()) return response()->json(['status' => 'ok']);

        return response()->json(['status' => 'not found']);
    }

    public function resetPassword(Request $request)
    {
        $data = (object) $request->only(['email', 'token', 'password']);

        $user = User::where('email', $data->email)->first();

        if (DB::table('password_resets')->where('email', $data->email)->where('token', $data->token)->count()) {
            $user->password = bcrypt($data->password);
            $user->save();

            DB::table('password_resets')->where('email', $data->email)->where('token', $data->token)->delete();
        }

        return response()->json(['status' => 'ok']);
    }

    public function getByProfile(Request $request, $profileId)
    {
        return User::join('profile_user', 'profile_user.user_id', '=', 'users.id')
            ->where('profile_user.profile_id', $profileId)
            ->select('users.*')->get();
    }
}
