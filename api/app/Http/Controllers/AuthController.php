<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * Login function
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request) {
        $request->validate([
            'email'     => 'required',
            'password'  => 'required|string'
        ]);

        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['status' => false, 'message' => 'invalid credentials'], 401);
        }

        $user           = \auth()->user();
        $tokenResult    = $user->createToken('Personal Access Token');

        $tokenResult->token->save();

        return response()->json([
            'status'        => true,
            'access_token'  => $tokenResult->accessToken,
            'expires_at'    => Carbon::parse($tokenResult->token->expires_at)->toDateString(),
            'user'          => $user->load(['profile'])
        ]);
    }

    /**
     * Logout function
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request) {
        DB::table('oauth_access_tokens')
            ->where('user_id', \auth()->user()->id)
            ->update([
                'revoked' => true
            ]);

        return response()->json([
            'status'    => true,
            'message'   => 'successfully logged out'
        ]);
    }

    /**
     * GetUser function
     *
     * @return void
     */
    public function getUser() {
        $user = \auth()->user();
        $user->load(['profile']);
        return response()->json($user);
    }
}
