<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alert;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Services\Alert\AlertService;
use Illuminate\Support\Facades\Log;

class AlertController extends ApiController
{
    use HttpResponse;

    /**
     * Constructor function
     */
    public function __construct(AlertService $service, Alert $model) {
        parent::__construct($service, $model);
    }

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            $alert = $this->service->store($request);

            $alert->save();

            DB::commit();

            return $alert;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }
}