<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Services\Faq\FaqService;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Traits\Storage;

class FaqController extends ApiController
{
    use HttpResponse, Storage;

    /**
     * Constructor function
     */
    public function __construct(FaqService $service, Faq $model)
    {
        parent::__construct($service, $model);
    }

    public function index()
    {
        return $this->showAll($this->service->index());
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $faq = $this->service->store($request);
            $faq->save();

            DB::commit();

            return $faq;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }
}
