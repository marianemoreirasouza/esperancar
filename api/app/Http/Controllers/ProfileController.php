<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Validators\ProfileStoreValidator;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    /**
     * Get Profiles function
     *
     * @return void
     */
    public function index(Request $request) {
        $profiles = Profile::whereRaw('1 = 1');

        if($request->search){
            $profiles->where(function($q) use($request){
                $q->where('type', 'like', "%{$request->search}%");
            });
        }

        if($request->sort){
            $order = explode(',', $request->sort);
            $profiles->orderBy($order[0], $order[1]);
        }

        return response()->json($profiles->paginate($request->per_page));
    }

    /**
     * Create Profile function
     *
     * @param StoreProfile $request
     * @return void
     */
    public function store(Request $request) {
        $validator = new ProfileStoreValidator($request->except('flows'));

        if(!$validator->passes())
            return $this->showResponse($validator->getErrors(), 400);

        $profile = new Profile;
        $profile->save();

        return response()->json(
            $profile
        );
    }

    /**
     * Edit Profile function
     *
     * @param Profile $profile
     * @param StoreProfile $request
     * @return void
     */
    public function put(Profile $profile, Request $request) {
        $validator = new ProfileStoreValidator($request->except('flows'));

        if(!$validator->passes())
            return $this->showResponse($validator->getErrors(), 400);

        $profile->save();

        return response()->json(
            $profile
        );
    }

    /**
     * Get Profile function
     *
     * @param Profile $profile
     * @return void
     */
    public function show(Profile $profile) {
        $profile->load(['']);

        return response()->json($profile);
    }

    /**
     * Get Profiles only in the trashed function
     *
     * @return void
     */
    public function trash(Request $request) {
        $profiles = Profile::onlyTrashed();

        if($request->search){
            $profiles->where(function($q) use($request){
                $q->where('type', 'like', "%{$request->search}%");
            });
        }

        if($request->sort){
            $order = explode(',', $request->sort);
            $profiles->orderBy($order[0], $order[1]);
        }

        return response()->json($profiles->paginate($request->per_page));
    }

    /**
     * Restore Profile function
     *
     * @param Profile $profile
     * @return void
     */
    public function restore(Profile $profile) {
        $profile->restore();

        return response()->json($profile);
    }

    /**
     * Delete Profile function
     *
     * @param Profile $profile
     * @return void
     */
    public function destroy(Profile $profile) {
        $profile->delete();

        return response()->json(['status' => true]);
    }
}
