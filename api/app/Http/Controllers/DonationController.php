<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Donation;
use App\Traits\AuthorizePermission;
use App\Traits\HttpResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Services\Donation\DonationService;
use Illuminate\Support\Facades\Log;
use App\Traits\Storage;
use stdClass;

class DonationController extends ApiController
{
    use HttpResponse, Storage;

    /**
     * Constructor function
     */
    public function __construct(DonationService $service, Donation $model)
    {
        parent::__construct($service, $model);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $donation = $this->service->store($request);
            $donation->save();
            if ($request->has('photo')) {
                $file = $this->putFile('donations', $donation->arquivo(), $request->photo);
                DB::table('donations')->where('id', $donation->id)
                    ->update(
                        ['file_id' => $file->id]
                    );
            }

            $date = date_parse_from_format("Y-m-d", $donation->date);

            $action = DB::table('actions')->whereYear('date', '=', $date["year"])->whereMonth('date', '=', $date["month"])->first();

            if (isset($action->id)) {
                DB::table('donations')->where('id', $donation->id)
                    ->update(
                        ['action_id' => $action->id]
                    );
            }

            if (!isset($action)) {
                return response()->json('error', 500);
            }

            DB::commit();
            return $donation;
        } catch (QueryException $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 500);
        }
    }

    public function getDonationsData(Request $request)
    {

        if (isset($request->user_id)) {
            $donations = DB::table("donations")->where('user_id', $request->user_id)
                ->select(DB::raw('sum(qty_bags) as `qty`'), DB::raw("DATE_FORMAT(date, '%m/%Y') new_date"),  DB::raw('YEAR(date) year, MONTH(date) month'))
                ->groupby('year', 'month')
                ->get();
        }else{
            $donations = DB::table("donations")
            ->select(DB::raw($request->type == "bags" ? 'sum(qty_bags) as `qty`' : 'count(id) as `qty`'), DB::raw("DATE_FORMAT(date, '%m/%Y') new_date"),  DB::raw('YEAR(date) year, MONTH(date) month'))
            ->groupby('year', 'month')
            ->get();
        }

        $response = new stdClass();
        $response->date = [];
        $response->qty = [];

        foreach ($donations as $donation) {
            array_push($response->date, $donation->new_date);
            array_push($response->qty, (int)$donation->qty);
        }


        return response()->json($response);
    }
}
