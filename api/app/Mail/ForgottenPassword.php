<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;

class ForgottenPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $password_reset, $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password_reset)
    {
        $this->password_reset = $password_reset;
        $this->user = User::where('email', $password_reset->email)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM'))
                    ->view('emails.user.forgotten_password');
    }
}
