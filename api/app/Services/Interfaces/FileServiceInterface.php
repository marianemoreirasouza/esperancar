<?php

namespace App\Services\Interfaces;

interface FileServiceInterface
{
    function save($path, $file);
    function saveAll($path, array $files);
    function delete();
}