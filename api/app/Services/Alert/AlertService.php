<?php

namespace App\Services\Alert;

use App\Models\Alert;
use App\Repositories\AlertRepository;
use App\Services\BaseService;

class AlertService extends BaseService
{
    protected $alertRepository;

    public function __construct() {
        $this->alertRepository = new AlertRepository(new Alert());
    }

    /**
     * Criar notificação
     * 
     * @return void
     */
    public function createNotification($data) {
        $this->alertRepository->create($data);
    }
}