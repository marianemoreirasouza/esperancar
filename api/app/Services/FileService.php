<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Repositories\Interfaces\FileRepositoryInterface;
use App\Services\Interfaces\FileServiceInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\File;

Class FileService implements FileServiceInterface
{    
    protected $fileRepository;

    public function __construct(FileRepositoryInterface $fileRepository) {
        $this->fileRepository = $fileRepository;
    }
    
    public function save($path, $file) {
        $storage = $this->putFile($path, $file);        
        $this->createFile($file, $storage);
    }

    public function saveAll($path, array $files) {        
        foreach($files as $k => $file)
            $this->save($path, $file);
    }

    public function delete() {

    }

    public function putFile($path, $contents, $options = [], $storage_local = null, $disk = 's3') {
        
        return Storage::disk($disk)->put(
            $this->pathStorageLocale($path, $this->defineStorageLocal($storage_local)),
            $contents,
            $options
        );
    }

    protected function createFile($file, $storage) {
        
        return $this->fileRepository->create([
            'user_id'       => auth()->user()->id,
            'name'          => $file->getClientOriginalName(),
            'storage_name'  => $storage,
            'size'          => $file->getSize(),
            'extension'     => $file->getClientOriginalExtension()
        ]);
    }

    protected function pathStorageLocale($path, $storage_local) {
        return $storage_local . '/' . $path . '/' . date('Y') . '/' . date('M');
    }

    protected function defineStorageLocal($storage_local) {
        return $storage_local ?: env('STORAGE_LOCALE', 'esperancar');
    }
}