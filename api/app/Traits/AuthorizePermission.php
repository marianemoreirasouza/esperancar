<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait AuthorizePermission
{
    /**
     * Verify Permission function
     *
     * @param string $permission
     * @param string $model
     * @param Request $request
     * @return void
     */
    private function permission(string $model) {
        $middleware = [];

        foreach ($this->resourceAbilityMap() as $method => $permission)
            $middleware["permission:{$permission},{$model}"][] = $method;

        foreach ($middleware as $middlewareName => $methods)
            $this->middleware($middlewareName)->only($methods);
    }
}