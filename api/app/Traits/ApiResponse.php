<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use function request;
use function response;
use function str_replace;
use function strpos;
use Illuminate\Pagination\Paginator;

trait ApiResponse
{
    protected function filter($response){
        $response = $this->_filter($response);
        $response = $this->_between($response);
        $response = $this->_like($response);
        $response = $this->_fields($response);
        $response = $this->_embed($response);
        $response = $this->_count_related($response);
        $response = $this->_groupBy($response);
        $response = $this->_sum($response);        

        if(request()->sort_by){ // priorize sort_by from request 
            $response->getQuery()->orders = null;
        }

        return $this->_sort($response);
    }
    protected function showAll($response,$code=200,$pluck=null,$get=false)
    {

        $response = $this->_filter($response);
        $response = $this->_between($response);
        $response = $this->_like($response);
        $response = $this->_fields($response);
        $response = $this->_embed($response);
        $response = $this->_count_related($response);
        $response = $this->_groupBy($response);
        $response = $this->_sum($response);

        if(request()->sort_by){ // priorize sort_by from request 
            $response->getQuery()->orders = null;
        }

        $response = $this->_sort($response);

        $data = $this->_paginate($response);// Apply Paginate

      return response()->json($data,$code)->withHeaders($data['pagination']);
    }

    protected function showOne($response,$code=200)
    {
            $response = $this->_embed($response);
            $data = $response->get();
            if (count($data)>0)
            $data = ['data'=>$data[0]];
            return response()->json($data,$code);
      
    }

    protected function showResponse($data,$code=200)
    {
       
       if (!is_object($data)){
        $data = ['data'=>$data, 'count'=>count($data)];
       }else{
        $data = ['data'=>$data];
       }
      

        return response()->json($data,$code);
        
    }


    protected function showDeleted($model)
    {
        return response()->json($model);
    }

    private function _pluck($response,$pluck)
    {
        if($pluck)
        {
            $response = $response->pluck($pluck)
                ->collapse();
        }
        return $response;
    }

    private function _sort($response)
    {

        if(request()->sort_by)
        {
            $attribute = request()->sort_by;
            if(strpos($attribute,'-') !== false)
            {
                $attribute = str_replace('-','',$attribute);
                return $response->orderBy($attribute, 'DESC');
            }
            return $response->orderBy($attribute,'ASC');
        }
        return $response;
    }

    private function _groupBy($response)
    {

        if(request()->group_by)
        {
          //  $response->select(['seller_id',DB::raw('COUNT('.request()->group_by.') as c'.request()->group_by)]);
          //  return $response->groupBy(request()->group_by);
        }
        return $response;
    }

    private function _sum($response)
    {

        if(request()->sum)
        {
            
           
        }
        return $response;
    }

    private function _fields($response)
    {
        if(request()->fields)
        {
            $fields = $this->getArray(request()->fields);
            return $response->select($fields);
        }
        return $response;
    }
    private function _embed($response)
    {
        if(request()->embed)
        {
            $embed = $this->getArray(request()->embed);
            
            foreach($embed as $item){
                $query = $this->getArray($item, '|');

                if(count($query) > 1){
                    $response->with([$query[0] => function($response) use($query){
                        $this->_filter($response, $query[1]);
                    }]);
                } else{
                    $response->with($query);
                }
            }            
        }
        return $response;
    }

    private function _like($response)
    {
        if(request()->like)
        {
            $like = $this->getArray(request()->embed);
            if (count($like)==2)
            return $response->where($like[0], 'like', '%' . $like[1] . '%');
        }
        return $response;
    }

    private function _filter($response, $query = null)
    {        
        $fields = $query ? $this->_configureArray($query) : request()->query();

        $actions = ['sort_by','embed','pluck','between','count_related','per_page','pages','page','sum','group_by','like','join'];

        foreach ($fields as $attribute => $value){

            if (!in_array($attribute,$actions) && substr($attribute, 0, 2) != "__"){

                $explode = explode(',',$value);
                if (count($explode) > 1){
                    $response->whereIn($attribute, $explode);
                }else{
                    if(strrpos($value,'%')>-1){
                        $response->where($attribute,'like',$value);
                    }else{
                        if (strrpos($attribute,'!')>-1){
                            $response->where(str_replace("!", "", $attribute),"!=",$value);
                        }else{
                            $response->where($attribute,$value);
                        }
                    }
                }
            }             
        }

        return $response;
    }

    private function _between($response)
    {
        if(request()->between)
        {
            $between = $this->getArray(request()->between);
            if(count($between) !== 3){
                return $response;
            }
            $column         = $between[0];
            $start_value    = $between[1];
            $end_value      = $between[2];

            return $response->whereDate($column,'>=', $start_value)
                ->whereDate($column,'<=',$end_value);
        }
        return $response;
    }
    private function _count_related($response){
        if(request()->count_related){
            $response  = $response->withCount(request()->count_related);
        }
        return $response;
    }

    private function _count($response){
        if(request()->count_related){
            $response  = $response->withCount(request()->count_related);
        }
        return $response;
    }

    private function _join($response){
        
        if(request()->join)
        {
            $join = $this->getArray(request()->join);

            return $response->join($join[0], $join[1], '=', $join[2]);
        }

        return $response;
    }
  
    private function getArray($fields, $separator = ',')
    {
        if($fields)
        {
            return explode($separator,$fields);
        }
        return null;
    }

    private function _configureArray($query){        
        $rows = explode(';', $query);
        $result = [];
        foreach($rows as $row){
            $columns = explode('=', $row);
            $result[$columns[0]] = $columns[1];
        }

        return $result;
    }

    protected function success($message, $code=200, $debug=''){
        $data =[
            'message'=>$message,
            'error'=>null,
            'code'=>$code,
            'debug'=>$debug
        ];
        $data = ['data'=>$data];
        return response()->json($data, $code);
    }

    protected function error($errors=[], $message='', $code=400,$debug=''){


        $data =[
            'message'=>$message,
            'error'=>$errors,
            'code'=>$code,
            'debug'=>$debug
        ];
        $data = ['data'=>$data];
        return response()->json($data, $code);
    }

    protected function showMessage($message, $code=200){
        return response()->json($message,$code);
    }

    protected function _paginate($response, $page = null){
        $perPage = request()->per_page ?: 30000;
        if($page){
            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });
        }
        $paginated = $response->paginate($perPage);
        $paginated->appends(request()->all());   
        
        $pages = $paginated->toArray();
        if(count($paginated->items()) == 0 && $paginated->currentPage()>1){
            return $this->_paginate($response, 1);
        } 
        return [
            'data'=>$paginated->items(),
            'pagination'=>[
                'from'=>$pages['from'],
                'to'=>$pages['to'],
                'total'=>$paginated->total(),
                'per_page'=>intval($paginated->perPage()),
                'last_page'=>$paginated->lastPage(),
                'current_page'=>$paginated->currentPage(),
                'next_page'=>$paginated->lastPage() < ($paginated->currentPage()+1) ? null : $paginated->currentPage()+1,
            ]
        ];

    }

   // to Paginate ArrayCollections

    protected function showPaginated($response){

        if (!is_array($response)){
          $response =  $response->toArray();
        }
        

        $page = LengthAwarePaginator::resolveCurrentPage();

        $perPage = request()->per_page ?: 15;
        $results = array_slice($response, ($page - 1) *$perPage, $perPage);

        $paginated = new LengthAwarePaginator($results, count($response), $perPage, $page, [
            'path'=>LengthAwarePaginator::resolveCurrentPath()
        ]);
        $paginated->appends(request()->all());  
        $pages = $paginated->toArray();

        $return =  [
            'data'=>$paginated->items(),
            'pagination'=>[
                'X-Pagination-first_page_url'=>$pages['first_page_url'],
                'X-Pagination-next_page_url'=>$pages['next_page_url'],
                'X-Pagination-prev_page_url'=>$pages['prev_page_url'],
                'X-Pagination-last_page_url'=>$pages['last_page_url'],
                //'Links'=>$this->_formatLinksToHeader($paginated),
                'X-Pagination-Total'=>$paginated->total(),
                'X-Pagination-PerPage'=>$paginated->perPage(),
                'X-Pagination-LastPage'=>$paginated->lastPage(),
                'X-Pagination-CurrentPage'=>$paginated->currentPage(),
            ]
        ];
        
         return response()->json($return['content'],200)
             ->withHeaders($return['header']);
    }

    private function _formatLinksToHeader($paginated)
    {
        $pages = $paginated->toArray();
        $first = "<{$pages['first_page_url']}>; rel='first_page_url'";
        $next = "<{$pages['next_page_url']}>; rel='next_page_url'";
        $prev = "<{$pages['prev_page_url']}>; rel='prev_page_url'";
        $last = "<{$pages['last_page_url']}>; rel='last_page_url'";

        $links = "{$first}, {$next}, {$prev}, {$last}";
        return $links;
    }
}