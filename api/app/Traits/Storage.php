<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage as StorageLaravel;
use App\Models\File;
use Illuminate\Support\Facades\Log;

trait Storage
{
    /**
     * Put File function
     *
     * @param $path
     * @param $contents
     * @param array $options
     * @param $storage_local
     * @param string $disk
     * @return void
     */
    public function putFile($path, $model, $contents, $comment = null, $options = [], $storage_local = null, $disk = 'public') {
        $fileStorage = StorageLaravel::disk($disk)->put(
            $this->pathStorageLocale($path, $this->defineStorageLocal($storage_local)),
            $contents,
            $options
        );

        return $this->createFile($fileStorage, $model, $contents, $comment);
    }

    /**
     * Delete File function
     *
     * @param $file_id
     * @param string $disk
     * @return void
     */
    public function deleteFile(File$file, $disk = 's3') {
        $this->deleteStorageFile($file, $disk);

        return $this->deleteDatabaseFile($file);
    }

    /**
     * Delete File in Storage function
     *
     * @param File $file
     * @param $disk
     * @return void
     */
    protected function deleteStorageFile(File $file, $disk) {
        return StorageLaravel::disk($disk)->delete($file->storage_name);
    }

    /**
     * Delete File in Database function
     *
     * @param File $file
     * @return void
     */
    protected function deleteDatabaseFile(File $file) {
        return $file->delete();
    }

    /**
     * Create File in Database function
     *
     * @param $fileStorage
     * @param $contents
     * @return void
     */
    protected function createFile($fileStorage, $model, $contents, $comment) {
        return $model->create([
            'user_id'       => auth()->user()->id,
            'name'          => $contents->getClientOriginalName(),
            'storage_name'  => $fileStorage,
            'size'          => $contents->getSize(),
            'extension'     => $contents->getClientOriginalExtension()
        ]);
    }

    /**
     * Structure Path Storage function
     *
     * @param $path
     * @param $storage_local
     * @return void
     */
    protected function pathStorageLocale($path, $storage_local) {
        return $storage_local . '/' . $path; 
    }

    /**
     * Define Storage Local function
     *
     * @param $storage_local
     * @return void
     */
    protected function defineStorageLocal($storage_local) {
        return $storage_local ?: env('STORAGE_LOCALE', 'esperancar');
    }
}