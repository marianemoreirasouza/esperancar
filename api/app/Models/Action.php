<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{

    protected $fillable = [
        'date'
    ];
    
    public function doacao() {
        return $this->hasMany(Donation::class);
    }
}
