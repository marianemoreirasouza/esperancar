<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{

    protected $fillable = [
        'user_id',
        'inform_id',
        'is_read',
        'title',
        'text'
    ];
    
    public function informs() {
        return $this->belongsTo(Inform::class, 'inform_id');
    }
}
