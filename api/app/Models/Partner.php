<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Partner extends Model
{

    protected $appends = ['url_img'];

    protected $fillable = [
        'name',
        'url',
        'file_id'
    ];

    public function arquivo() {
        return $this->belongsTo(File::class, 'file_id');
    }
    
    public function getUrlImgAttribute(){
        if(isset($this->arquivo()->first()->storage_name))
            return $this->arquivo()->first()->storage_name;
        
        return null;
    }
}
