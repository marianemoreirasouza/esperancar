<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Faq extends Model
{

    protected $table = 'faq';

    protected $fillable = [
        'text'
    ];
}
