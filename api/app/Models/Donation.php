<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{

    protected $appends = ['blood_type'];

    protected $fillable = [
        'user_id',
        'action_id',
        'file_id',
        'qty_bags',
        'date'
    ];

    public function doador() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function acao() {
        return $this->belongsTo(Action::class, 'action_id');
    }

    public function arquivo() {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function getBloodTypeAttribute(){
        if(isset($this->doador()->first()->blood_type))
            return $this->doador()->first()->blood_type;
        
        return null;
    }
}
