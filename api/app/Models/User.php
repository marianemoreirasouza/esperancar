<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use SoftDeletes, HasApiTokens, Notifiable;

    protected $appends = ['qty_donations', 'last_donation'];

    protected $fillable = [
        'name', 'cpf', 'sex', 'main_phone', 'msg_phone', 'email', 'birthdate', 'blood_type', 'address', 'password', 'alerts'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile() {
        return $this->hasOne(Profile::class);
    }

    public function donations() {
        return $this->hasMany(Donation::class, 'user_id');
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getQtyDonationsAttribute(){
        if($this->donations()->get() != null)
            return sizeof($this->donations()->get());
        
        return null;
    }

    public function getLastDonationAttribute(){
        if(sizeof($this->donations()->get()) > 0)
            return $this->donations()->where('date', $this->donations()->max('date'))->orderBy('date','desc')->first()->date;

        return null;
    }
    
}
