<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\AwsS3;

class File extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'storage_name',
        'size',
        'extension'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
