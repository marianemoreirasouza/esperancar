<?php

namespace App\Helpers;

use Carbon\Carbon;

class Date {

    public static function addYearFromDate($date) {

        return Carbon::createFromFormat('Y-m-d', $date)->addYear()->toDateString();

    }


}
