<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Log;

class FormatJson {

    public static function Paginated($data, $page, $per_page) {
        $total = count($data->all());
        $from = $page*$per_page-($per_page-1);
        $to = $from+count($data->forPage($page, $per_page))-1;
        $data_aux = $data->forPage($page, $per_page);

        if(count($data_aux)==0 && $page > 1){
            $data_aux = self::RecalcPage($from,$to,$data,$page,$per_page);
        }

        return [
            'data'=>$data_aux,
            'from'=>$from,
            'to'=>$to,
            'total'=>$total,
            'per_page'=>(int)$per_page,
            'current_page'=>(int)$page,
        ];
    }

    private static function RecalcPage(&$from,&$to,$data,&$page,$per_page){
        $page_aux = $page;
        $area = [1,$page-1];
        $contador = 0;
        
        $ok = false;
        while(!$ok){
            $aux = (int)(($area[0]+$area[1])/2)+1;
            $data_aux = $data->forPage($aux, $per_page);
            if(count($data_aux)>0){
                $area[0] = $aux;
            } else {
                $area[1] = $aux-1;
            }
            if($area[1]==$area[0]){
                $page = $area[0];
                $data_aux = $data->forPage($page, $per_page);
                $ok = true;
            }
        }
        $from = $page*$per_page-($per_page-1);
        $to = $from+count($data_aux)-1;

        return $data_aux;
    }


}
