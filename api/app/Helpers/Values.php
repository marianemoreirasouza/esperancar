<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Log;

use App\Models\Action;

class Values {

    public static function CreateMarginObject(){
        return (object) array(
            'value' => 0,
            'expense' => 0,
            'expire' => 0,
            'margin' => 0,
            'marginExpected' => 0,
            'expenseExpected' => 0,
            'valueReal' => 0,
            'expenseReal' => 0,
            'expireReal' => 0,
            'marginReal' => 0,
            'marginExpectedReal' => 0,
            'expenseExpectedReal' => 0,
            'isOtherCurrency' => false
        );
    }

    public static function GetAllValuesMarketingPlan($mp) {
        $margin = self::CreateMarginObject();
        foreach ($mp->actions as $action) {
            $margin = self::SumMargin($margin,self::GetMarginActionById($action->id));
        }
        return $margin;
    }

    public static function SumMargin($margin1,$margin2){
        $aux = array(
            'value' => $margin1->value + $margin2->value,
            'expense' => $margin1->expense + $margin2->expense,
            'expire' => $margin1->expire + $margin2->expire,
            'margin' => $margin1->margin + $margin2->margin,
            'marginExpected' => $margin1->marginExpected + $margin2->marginExpected,
            'expenseExpected' => $margin1->expenseExpected + $margin2->expenseExpected,
            'valueReal' => $margin1->valueReal + $margin2->valueReal,
            'expenseReal' => $margin1->expenseReal + $margin2->expenseReal,
            'expireReal' => $margin1->expireReal + $margin2->expireReal,
            'marginReal' => $margin1->marginReal + $margin2->marginReal,
            'marginExpectedReal' => $margin1->marginExpectedReal + $margin2->marginExpectedReal,
            'expenseExpectedReal' => $margin1->expenseExpectedReal + $margin2->expenseExpectedReal,
            'isOtherCurrency' => $margin1->isOtherCurrency || $margin2->isOtherCurrency,
        );
        
        //$aux['mpp'] = (isset($margin1->mpp)?$margin1->mpp:0) + (isset($margin2->mpp)?$margin2->mpp:0);
        //$aux['mppr'] = (isset($margin1->mppr)?$margin1->mppr:0) + (isset($margin2->mppr)?$margin2->mppr:0);
            
        return (object) $aux;
    }

    private static function calcMedia($valor, $allValue){
        // pegar pesos
        $w = collect($allValue)->map(function($q) use($valor){
            if($valor==0)
            $peso = 0;
            else
            $peso = ($q->actual_value*100)/$valor;
            $cotacao = self::getCotacao($q->fund);
            $res = $peso*$cotacao;
            return (object)[
                'peso'=>$peso,
                'res'=>$res
            ];
        });

        if($w->sum('peso')==0)
        return 0;
        return $w->sum('res')/$w->sum('peso');
    }

    public static function getCotacao($fund){
        if(isset($fund) && isset($fund->value_cotacao)){
            return $fund->value_cotacao==0?1:$fund->value_cotacao;
        }
        return 1;
    }

    public static function GetMarginActionById($id){
        $action = Action::with('allValues.fund','expenseDescriptions')
            ->where('id',$id)->first();

        $resume = self::CreateMarginObject();

        if($action->currency != 1)
        $resume->isOtherCurrency = true;

        //$aux_mpp = 0;// margem para plano
        //$aux_mppr = 0;// margem para plano em real

        // somando valores
        foreach ($action->allValues as $values) {
            $resume->expire += $values->funds_expire_value == null ? 0 : $values->funds_expire_value;
            $resume->value += $values->actual_value;

            $resume->expireReal += $values->funds_expire_value == null ? 0 : $values->funds_expire_value*self::getCotacao($values->fund);
            $resume->valueReal += $values->actual_value*self::getCotacao($values->fund);
        }

        // pegar o expense da margem
        if(($action->status_id >= 5 && $action->status_id <= 13) || $action->status_id == 21 || $action->status_id == 22){
            $resume->expense = $action->real_value;
            $resume->expenseExpected = $action->expected_value;
            
            // pegando valores covertidos para real caso estejam em outra moeda
            $media = self::calcMedia($action->expected_value,$action->allValues);
            $resume->expenseReal = $action->real_value * $media;
            $resume->expenseExpectedReal = $action->expected_value * $media;
        } else {
            foreach ($action->expenseDescriptions as $expenseDescription) {
                $resume->expense += ($expenseDescription->vinculated_nf == "0")?
                (
                    $expenseDescription->value/(
                        $expenseDescription->value_cotacao?
                        $expenseDescription->value_cotacao:1
                    )
                ):0;

                // pegando valores covertidos para real caso estejam em outra moeda
                $media = self::calcMedia($action->expected_value,$action->allValues);
                $resume->expenseReal += ($expenseDescription->vinculated_nf == "0")?
                (
                    $expenseDescription->value_cotacao?
                    $expenseDescription->value:$expenseDescription->value*$media
                ):0;
            }
        }

        $resume->value += $resume->expire;
        $resume->valueReal += $resume->expireReal;
        
        // calculando margem
        $resume->margin = $resume->value - $resume->expense - $resume->expire - ($resume->expense > 0 ? 0 : $resume->value);
        //$resume->mpp = $aux_mpp - $resume->expense - $resume->expire;
        $resume->marginExpected = ($resume->margin > 0 ? 0 : $resume->value - $resume->expenseExpected - $resume->expire);
        $resume->marginReal = $resume->valueReal - $resume->expenseReal - $resume->expireReal - ($resume->expenseReal > 0 ? 0 : $resume->valueReal);
        //$resume->mppr = $aux_mppr - $resume->expenseReal - $resume->expireReal;
        $resume->marginExpectedReal = ($resume->marginReal > 0 ? 0 : $resume->valueReal - $resume->expenseExpectedReal - $resume->expireReal);
        
        return $resume;
    }
}
