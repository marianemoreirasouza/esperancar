<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class AwsS3
{
    public static function getPreSignedURI($filePath = null) {
        if(!$filePath) return false;

        $client = Storage::disk('s3')->getDriver()->getAdapter()->getClient();

        $expiry = "+6 days";

        $command = $client->getCommand('GetObject', [
            'Bucket' => Config::get('filesystems.disks.s3.bucket'),
            'Key'    => $filePath
        ]);
       
        $request = $client->createPresignedRequest($command,$expiry );

        return (string) $request->getUri();
    }
}
