<?php

namespace App\Repositories;

use App\Models\Alert;
use App\Repositories\Repository;

class AlertRepository extends Repository
{
    public function __construct(Alert $model) {
        $this->model = $model;
    }
}
