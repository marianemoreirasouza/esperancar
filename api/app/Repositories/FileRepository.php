<?php

namespace App\Repositories;

use App\Models\File;
use App\Repositories\Repository;
use App\Repositories\Interfaces\FileRepositoryInterface;

class FileRepository extends Repository implements FileRepositoryInterface
{
    public function __construct(File $model) {
        $this->model = $model;
    }
}