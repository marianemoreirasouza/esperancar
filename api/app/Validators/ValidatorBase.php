<?php namespace App\Validators;

use Validator;
use Illuminate\Http\Request;

abstract class ValidatorBase {

	protected $input;
	protected $errors;
	protected $messages = [];
	protected $id;
	protected $friendlyNames =[];

	public function __construct($input, $id = null) {
		$this->input = $input;
		$this->id = $id;
    }

	public function passes(){

        $validation = Validator::make($this->input, $this->rules, $this->messages, $this->friendlyNames);

        if($validation->passes()) return true;

		$this->errors = $validation->messages();

        return false;
	}
	public function getErrors() {

		return $this->errors->all();
	}
}
