<?php namespace App\Validators;

class UserStoreValidator extends ValidatorBase {
    protected $rules = [
		'firstname' => 'required',
        'lastname'  => 'required',
        'email'     => 'required|email:rfc|unique:users',
        'phone'     => '',
        'alias'     => '',
        'language'  => 'required|numeric|exists:languages,id',
        'profile'   => 'required|numeric|exists:profiles,id'
    ];
}