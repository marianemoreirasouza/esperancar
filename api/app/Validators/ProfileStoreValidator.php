<?php namespace App\Validators;

class ProfileStoreValidator extends ValidatorBase {
    protected $rules = [
		'name'              => 'required',
        'area_id'           => 'required|exists:areas,id',
        'profile_type_id'   => 'required|exists:profile_types,id',
        'permissions'       => 'array'
    ];
}