<?php namespace App\Validators;

class UserUpdateValidator extends ValidatorBase {
    protected $rules = [
		'firstname' => 'required',
        'lastname'  => 'required',
        'email'     => 'required',
        'phone'     => '',
        'alias'     => '',
        'language'  => 'required|numeric|exists:languages,id'
    ];
}