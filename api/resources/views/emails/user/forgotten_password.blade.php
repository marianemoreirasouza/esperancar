<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Definição de senha</title>
</head>

<body>
    <table width="650" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
            <tr>
                <td height="260"><img src="" width="750" height="315" class="p100" style="-ms-interpolation-mode: bicubic; border: 0; display: block; outline: 0; text-decoration: none; width: 750px;" border="0" / /></td>
            </tr>
            <tr>
                <td>
                    <table width="650" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td style="width:62px; height:379px; background-color: #fff;">&nbsp;</td>
                                <td style="width:379px; height:379px; background-color: #fff; font-family: 'Montserrat', Tahoma; font-size:15px; color:#464646">
                                    <b style="font-size:30px;">Olá, {{ $user->name }} </b><Br><Br>
                                    <p style="font-family: 'Arial', sans-serif; font-size: 11pt; color: #464646; font-weight: 400; line-height: 14pt">
                                        Para redefinir sua senha <a href="{{ env("URL_FRONT") }}resetpassword/{{ $password_reset->token }}/{{ $password_reset->email }}" target="_blank" style="font-family: 'Arial', sans-serif; font-size: 11pt; color: #464646;">clique
                                            aqui</a>.
                                    </p>
                                </td>
                                <td style="width:72px; height:379px; background-color: #fff;">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
            <tr>
                <td>
                    <table style="background-color: #292526; margin: 0; mso-table-lspace: 0; mso-table-rspace: 0; padding: 0; width: 100%;" cellspacing="0" cellpadding="0" border="0" bgcolor="#292526" width="100%">

                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="80%" border="0" cellspacing="0" cellpadding="2">

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="left" style="color: #fff; font-size: 9px; font-family: 'Open Sans', sans-serif; line-height: 22px; mso-line-height-rule: exactly; ">
                                <strong>&copy; Copyright Esperancar 2020</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tr>
        </tbody>
    </table>
</body>

</html>