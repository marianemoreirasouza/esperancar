<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignsKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('action_id')->references('id')->on('actions');
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('file_id')->references('id')->on('files');
        });

        Schema::table('informs', function (Blueprint $table) {
            $table->foreign('file_id')->references('id')->on('files');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function(Blueprint $table) {
			$table->dropForeign('user_id');
        });
        
        Schema::table('donations', function(Blueprint $table) {
			$table->dropForeign('user_id');
        });
        
        Schema::table('donations', function(Blueprint $table) {
			$table->dropForeign('action_id');
		});

        Schema::table('donations', function(Blueprint $table) {
			$table->dropForeign('file_id');
        });
        
        Schema::table('informs', function(Blueprint $table) {
			$table->dropForeign('file_id');
		});
    }
}
