<?php

Route::post('auth/login', 'AuthController@login');
Route::post('register-password/verify-token', 'UserController@verifyTokenRegisterPassword');
Route::post('register-password', 'UserController@registerPassword');
Route::post('forgotten-password', 'UserController@requestForgottenPassword');
Route::post('reset-password/verify-token', 'UserController@verifyTokenResetPassword');
Route::post('reset-password', 'UserController@resetPassword');

Route::get('partners-data', 'PartnerController@index');
Route::get('faq-data', 'FaqController@index');
Route::post('users-create', 'UserController@store');

Route::group(['middleware' => ['auth:api', 'headers']], function () {
    Route::get('auth/logout', 'AuthController@logout');
    Route::get('auth/user', 'AuthController@getUser');

    Route::resource('users', 'UserController');
    Route::resource('actions', 'ActionController');
    Route::resource('informs', 'InformController');
    Route::resource('alerts', 'AlertController');
    Route::resource('donations', 'DonationController');
    Route::resource('faq', 'FaqController');
    Route::resource('partners', 'PartnerController');

    Route::get('donations-data', 'DonationController@getDonationsData');
    Route::get('users-with-profile', 'UserController@getWithProfile');
    Route::get('actions-with-users', 'ActionController@getWithUsers');
});
