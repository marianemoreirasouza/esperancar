import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainDrawer from './src/stacks/MainDrawer';

export default () => {
  return (
    <NavigationContainer>
      <MainDrawer />
    </NavigationContainer>
  );
}
