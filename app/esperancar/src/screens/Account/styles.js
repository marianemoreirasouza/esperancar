import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: #fff;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px
`;

export const InputArea = styled.View`
    width: 100%;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: #D90429;
    border-radius: 5px;
    justify-content: center;
    align-items: center;
    padding: 10px;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #FFF;
    font-weight: bold;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;

export const Span = styled.Text`
    font-size: 14px;
    color: #D90429;
    font-weight: bold;
    margin-left: 10px;
`;

export const Image = styled.Image`
    top: 0px;
    width: 20%;
    height: 10%;
`;