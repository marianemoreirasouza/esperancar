import React, { useState, useEffect } from 'react';
import {
    Container,
    InputArea,
    CustomButton,
    CustomButtonText,
    CustomText,
    Span
} from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import SignInput from '../../components/SignInput';
import InviteFriend from '../../components/InviteFriend';

const Account = ({invite, setInvite}) => {

    const [name, setName] = useState('');
    const [bloodType, setBloodType] = useState('');
    const navigation = useNavigation();

    useEffect(() => {
        const checkData = async () => {
            setName(await AsyncStorage.getItem('name'));
            setBloodType(await AsyncStorage.getItem('blood_type'));
        }
        checkData();
    }, []);

    const handleClick = () => {
        navigation.reset({
            routes: [{ name: 'Editar Conta' }]
        });
    }

    return (
        <Container>
            <CustomText>Minha Conta</CustomText>
            <InputArea>
                <Span>Nome</Span>
                <SignInput
                    value={name}
                    editable={false}
                />
                <Span>Tipo Sanguíneo</Span>
                <SignInput
                    value={bloodType}
                    editable={false}
                />
            </InputArea>
            <CustomButton onPress={handleClick}>
                <CustomButtonText>Editar Conta</CustomButtonText>
            </CustomButton>
            <InviteFriend invite={invite} setInvite={setInvite}/>
        </Container>
    );
};

export default Account;