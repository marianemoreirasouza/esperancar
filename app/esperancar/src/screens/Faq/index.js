import React, { useState, useEffect, useCallback } from 'react';
import {
    Container,
    CustomText,
    TextFaq,
    ScrollView
} from './styles';
import InviteFriend from '../../components/InviteFriend';
import Api from '../../services/faq-service';
import { Text, Linking, TouchableOpacity, TouchableHighlight } from 'react-native';

const Faq = ({ invite, setInvite }) => {

    const [faqText, setFaqText] = useState("");
    const B = (props) => <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{props.children}</Text>
    const reactStringReplace = require('react-string-replace');

    const A = ({ url, children }) => {
        const handlePress = useCallback(async () => {
          const supported = await Linking.canOpenURL(url);
      
          if (supported) {
            await Linking.openURL(url);
          } else {
            Alert.alert(`A seguinte URL não pôde ser aberta: ${url}`);
          }
        }, [url]);
      
        return <Text style={{ fontWeight: 'bold', color: "#EF233C", textDecorationLine: 'underline' }} onPress={handlePress}>{children}</Text>;
      };
      

    useEffect(() => {
        const checkData = async () => {
            let json = await Api.getFaq();
            if (json.data) {
                let text = json.data[json.data.length - 1].text;
                let replacedText = reactStringReplace(text, /<b>(.*?)<\/b>/g, (match, i) => (
                    <B key={match + i}>{match}</B>
                  ));
                replacedText = reactStringReplace(replacedText, /<a>(.*?)<\/a>/g, (match, i) => (
                    <A key={match + i} url={match}>{match}</A>
                  ));
                setFaqText(replacedText);
            }
        }
        checkData();
    }, []);

    return (
        <Container>
            <CustomText>FAQ</CustomText>
            <ScrollView>
                <TextFaq>{faqText}</TextFaq>
            </ScrollView>
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

export default Faq;