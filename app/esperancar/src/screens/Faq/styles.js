import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const ScrollView = styled.ScrollView`
`;

export const InputArea = styled.View`
    width: 100%;
    padding: 25px;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: red;
    border-radius: 5px;
    justify-content: center;
    margin-left: 20px
    align-items: center;
    padding: 10px;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #FFF;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
    margin-left: 40px;
`;

export const TextFaq = styled.Text`
    font-size: 15px;
    color: #000;
    padding: 0px 40px;
    margin-bottom: 40px;
`;

export const Span = styled.Text`
    font-size: 14px;
    color: red;
    font-weight: bold;
    margin-left: 20px;
`;

export const SwitchButton = styled.Switch`
`;