import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { maskDocument, maskPhone } from '../../helpers/document';

import {
    Container,
    InputArea,
    CustomButton,
    CustomButtonText,
    SignMessageButton,
    SignMessageButtonText,
    SignMessageButtonTextBold,
    ScrollView,
    Span,
    BaseText,
    CustomText
} from './styles';

import SignInput from '../../components/SignInput';
import SignSelect from '../../components/SignSelect';
import SignDatepicker from '../../components/SignDatepicker';
import InviteFriend from '../../components/InviteFriend';
import { parseDate } from '../../helpers/date';

import Api from '../../services/login-service';

const SignUp = ({ invite, setInvite }) => {
    const navigation = useNavigation();

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [cpf, setCPF] = useState('');
    const [blood_type, setBloodType] = useState('');
    const [sex, setSex] = useState('M');
    const [main_phone, setMainPhone] = useState('');
    const [msg_phone, setMsgPhone] = useState('');
    const [birthdate, setBirthDate] = useState('');
    const [address, setAddress] = useState('');

    const handleSignClick = async () => {
        if (name != '' && email != '' && password != '') {
            let json = await Api.signUp(name, cpf, sex, main_phone, msg_phone, blood_type, email, parseDate(birthdate), address, password);
            if (json.name) {

                alert("Usuário criado com sucesso! Por favor faça login")

                navigation.reset({
                    routes: [{ name: 'Entrar' }]
                });

            } else {
                alert("Erro: " + json.error);
            }
        } else {
            alert("Preencha os campos");
        }
    }

    const handleMessageButtonClick = () => {
        navigation.reset({
            routes: [{ name: 'Entrar' }]
        });
    }

    return (

        <ScrollView>
            <Container>
                <CustomText>Seja um Doador</CustomText>
                <InputArea>
                    <Span>Nome</Span>
                    <SignInput
                        placeholder="Digite seu nome"
                        value={name}
                        onChangeText={t => setName(t)}
                        editable={true}
                    />
                    <Span>CPF</Span>
                    <SignInput
                        placeholder="Digite seu CPF"
                        value={maskDocument(cpf)}
                        onChangeText={t => setCPF(t)}
                        editable={true}
                    />
                    <Span>Tipo Sanguíneo</Span>
                    <SignSelect
                        value={blood_type}
                        setValue={setBloodType}
                        options={[{ 'label': 'A+', 'value': 'A+' },
                        { 'label': 'A-', 'value': 'A-' },
                        { 'label': 'B+', 'value': 'B+' },
                        { 'label': 'B-', 'value': 'B-' },
                        { 'label': 'AB+', 'value': 'AB+' },
                        { 'label': 'AB-', 'value': 'AB-' },
                        { 'label': 'O+', 'value': 'O+' },
                        { 'label': 'O-', 'value': 'O-' }]}>
                    </SignSelect>
                    <Span>Sexo</Span>
                    <SignSelect
                        value={sex}
                        setValue={setSex}
                        options={[{ 'label': 'Masculino', 'value': 'M' },
                        { 'label': 'Feminino', 'value': 'F' }]}>
                    </SignSelect>
                    <Span>Telefone Principal</Span>
                    <SignInput
                        placeholder="Digite seu telefone principal"
                        value={maskPhone(main_phone)}
                        numeric={true}
                        onChangeText={t => setMainPhone(t)}
                        editable={true}
                    />
                    <Span>Telefone de Recados</Span>
                    <SignInput
                        placeholder="Digite seu telefone para recados"
                        value={maskPhone(msg_phone)}
                        numeric={true}
                        onChangeText={t => setMsgPhone(t)}
                        editable={true}
                    />
                    <Span>Data de Nascimento</Span>
                    <SignDatepicker date={birthdate} setDate={setBirthDate} />
                    <Span>Endereço</Span>
                    <SignInput
                        placeholder="Digite seu endereço"
                        value={address}
                        onChangeText={t => setAddress(t)}
                        editable={true}
                    />
                    <Span>E-mail</Span>
                    <SignInput
                        placeholder="Digite seu e-mail"
                        value={email}
                        onChangeText={t => setEmail(t)}
                        editable={true}
                    />
                    <Span>Senha</Span>
                    <SignInput
                        placeholder="Digite sua senha"
                        value={password}
                        onChangeText={t => setPassword(t)}
                        password={true}
                        editable={true}
                    />

                    <CustomButton onPress={handleSignClick}>
                        <CustomButtonText>Cadastrar</CustomButtonText>
                    </CustomButton>
                </InputArea>

                <SignMessageButton onPress={handleMessageButtonClick}>
                    <BaseText>
                        <SignMessageButtonText>Já possui uma conta?</SignMessageButtonText>
                        <SignMessageButtonTextBold> Faça Login</SignMessageButtonTextBold>
                    </BaseText>
                </SignMessageButton>
                <InviteFriend invite={invite} setInvite={setInvite} />
            </Container>
        </ScrollView>
    );
}

export default SignUp;