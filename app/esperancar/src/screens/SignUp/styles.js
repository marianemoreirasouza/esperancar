import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: #fff;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px;
`;

export const ScrollView = styled.ScrollView`
`;

export const InputArea = styled.View`
    width: 100%;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: #D90429;
    border-radius: 5px;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #FFF;
    font-family: "Roboto-Bold"
`;

export const SignMessageButton = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: center;
    margin-top: 50px;
    margin-bottom: 20px;
`;

export const SignMessageButtonText = styled.Text`
    font-size: 16px;
    color: #000;
`;

export const SignMessageButtonTextBold = styled.Text`
    font-size: 16px;
    color: #EF233C;
    font-family: "Roboto-Bold"
    margin-left: 5px;
`;

export const Span = styled.Text`
    font-size: 14px;
    color: #D90429;
    font-weight: bold;
    margin-left: 10px;
`;

export const BaseText = styled.Text`
    font-family: "Roboto-Regular";
`;