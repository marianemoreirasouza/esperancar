import React, { useState } from 'react';
import {
    Container,
    InputArea,
    CustomButton,
    CustomButtonText,
    SignMessageButton,
    SignMessageButtonText,
    SignMessageButtonTextBold,
    Image,
    Span,
    BaseText
} from './styles';
import Api from '../../services/login-service';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import SignInput from '../../components/SignInput';
import LogoEsperancar from '../../assets/logo.png';
import InviteFriend from '../../components/InviteFriend';

const SignIn = ({ invite, setInvite, setToken }) => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigation = useNavigation();

    const handleSignClick = async () => {
        if (email != '' && password != '') {
            let json = await Api.signIn(email, password);
            if (json.access_token && json.user.profile.type == "Doador") {
                await AsyncStorage.setItem('token', json.access_token);
                await AsyncStorage.setItem('name', json.user.name);
                await AsyncStorage.setItem('id', json.user.id.toString());
                await AsyncStorage.setItem('blood_type', json.user.blood_type);
                await AsyncStorage.setItem('alerts', json.user.alerts.toString());
                await AsyncStorage.setItem('sex', json.user.sex);
                if (json.user.last_donation) {
                    await AsyncStorage.setItem('last_donation', json.user.last_donation);
                } else {
                    await AsyncStorage.setItem('last_donation', '0');
                }
                setToken(json.access_token);

                navigation.reset({
                    routes: [{ name: 'Dashboard' }]
                });
            } else {
                alert('E-mail e/ou senha errados!');
            }

        } else {
            alert("Preencha todos os campos!");
        }
    }

    const handleMessageButtonClick = () => {
        navigation.reset({
            routes: [{ name: 'Seja um Doador' }]
        });
    }

    return (
        <Container>
            <Image source={LogoEsperancar} />
            <InputArea>
                <Span>E-mail</Span>
                <SignInput
                    placeholder="Digite seu e-mail"
                    value={email}
                    onChangeText={t => setEmail(t)}
                    editable={true}
                />
                <Span>Senha</Span>
                <SignInput
                    placeholder="Digite sua senha"
                    value={password}
                    onChangeText={t => setPassword(t)}
                    password={true}
                    editable={true}
                />
                <CustomButton onPress={handleSignClick}>
                    <BaseText>
                        <CustomButtonText>Login</CustomButtonText>
                    </BaseText>
                </CustomButton>
            </InputArea>

            <SignMessageButton onPress={handleMessageButtonClick}>
                <BaseText>
                    <SignMessageButtonText>Ainda não possui uma conta?</SignMessageButtonText>
                    <SignMessageButtonTextBold> Cadastre-se</SignMessageButtonTextBold>
                </BaseText>
            </SignMessageButton>
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
}

export default SignIn;