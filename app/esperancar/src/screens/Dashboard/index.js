import React, { useState, useEffect } from 'react';
import { Container, CustomText, ChartText, Column, CenterContainer, Label, Count, SecondaryText, SecondaryTextBold } from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import InviteFriend from '../../components/InviteFriend';
import RoundButton from '../../components/RoundButton';
import BloodDrop from '../../components/BloodDrop';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Api from '../../services/donation-service';
import { LineChart } from "react-native-chart-kit";
import { Dimensions } from "react-native";

const Dashboard = ({ invite, setInvite }) => {

    const [name, setName] = useState('');
    const [blood_type, setBloodType] = useState('');
    const [userId, setUserId] = useState('');
    const [donations, setDonations] = useState({ data: [], qty: [] });
    const [donationsCount, setDonationsCount] = useState(0);
    const [donationsBags, setDonationsBags] = useState(0);
    const [nextDonation, setNextDonation] = useState(0);

    const CalcNextDonation = (last_donation, sex) => {

        let date = new Date();
        let diffDays = Math.ceil(Math.abs(last_donation - date) / (1000 * 60 * 60 * 24)) - 1

        if (sex == 'F')
            return 90 - diffDays;
        else
            return 60 - diffDays;
    }

    useEffect(() => {

        const checkData = async () => {
            setName(await AsyncStorage.getItem('name'));
            setBloodType(await AsyncStorage.getItem('blood_type'));

            if (userId == '') setUserId(await AsyncStorage.getItem('id'));
            let response = await Api.getDonations(userId);
            if (response.data) {
                setDonationsCount(response.data.length);
                let countBags = 0;
                response.data.map((donation, k) => {
                    countBags += donation.qty_bags;
                });
                setDonationsBags(countBags);
            }

            if (userId) {
                let data = await Api.getDonationsData(userId);
                if (data) {
                    setDonations(data);
                }
            }

            let last_donation = await AsyncStorage.getItem('last_donation');
            if (last_donation !== 0) {
                let sex = new Date(await AsyncStorage.getItem('sex'));
                setNextDonation(CalcNextDonation(last_donation, sex));
            } else {
                setNextDonation(0);
            }
        }
        checkData();
    }, [userId]);

    return (
        <Container>
            <CustomText>Olá, {name}</CustomText>
            {nextDonation > 0 &&
                <SecondaryText>A próxima doação poderá ser feita em <SecondaryTextBold>{nextDonation} dias</SecondaryTextBold></SecondaryText>
            }
            <CenterContainer>
                <Column>
                    <Label>Total de Doações Realizadas</Label>
                    <Count>{donationsCount ?? 0}</Count>
                </Column>
                <Column><BloodDrop blood_type={blood_type} /></Column>
                <Column>
                    <Label>Bolsas de sangue doadas</Label>
                    <Count>{donationsBags ?? 0}</Count>
                </Column>
            </CenterContainer>
            {donations.date && donations.date.length > 0 ?
                <>
                    <ChartText>Bolsas doadas por ação</ChartText>
                    <LineChart
                        data={{
                            labels: [...donations.date],
                            datasets: [
                                {
                                    data: [...donations.qty]
                                }
                            ]
                        }}
                        width={Dimensions.get("window").width - 80}
                        height={Dimensions.get("window").height / 3}
                        segments={donations.date.length}
                        chartConfig={{
                            backgroundColor: "#2b2d42",
                            backgroundGradientFrom: "#2b2d42",
                            backgroundGradientTo: "#2b2d42",
                            decimalPlaces: 0,
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "8",
                                strokeWidth: "2",
                                stroke: "#D90429"
                            }
                        }}
                        bezier
                        style={{
                            borderRadius: 10
                        }}
                    />
                </>
                :
                <RoundButton icon={faPlus} screen={"Doar"} ht={Dimensions.get("window").height * 0.025} />
            }
            {/* <RoundButton icon={faTint} screen={"Minhas Doações"} ht={10} /> */}
            {/* <RoundButton icon={faPlus} screen={"Doar"} ht={Dimensions.get("window").height * 0.025} /> */}
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};


export default Dashboard;