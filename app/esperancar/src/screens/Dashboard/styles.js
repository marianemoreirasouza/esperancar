import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px;
`;

export const CenterContainer = styled.View`
    flex: 1;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-content: center;
    align-items: center;
    margin-top: 80px;
`;

export const Column = styled.View`
    justify-content: center;
    align-content: center;
    align-items: center;
    width: 33%;
`;

export const Image = styled.Image`
    width: 50%;
    height: 50%;
`;

export const LoadingIcon = styled.ActivityIndicator`
    margin-top: 50px;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    width: 100px;
    background-color: red;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    margin-top: 20px;
`;

export const CustomButtonText = styled.Text`
    color: #FFF;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    align-self: flex-start;
    margin-top: 20px;
`;

export const SecondaryText = styled.Text`
    font-size: 15px;
    color: #000;
    align-self: flex-start;
    margin-top: 20px;
    margin-bottom: 50px;
`;

export const SecondaryTextBold = styled.Text`
    font-weight: bold
`;

export const ChartText = styled.Text`
    font-size: 24px;
    color: #000;
    font-weight: bold;
    align-self: flex-start;
    margin-top: 50px;
    margin-bottom: 15px;
`;

export const Label = styled.Text`
    font-size: 16px;
    color: #000;
    margin: 10px;
    font-family: "Roboto-Regular";
    align-self: center;
    text-align: center;
`;

export const Count = styled.Text`
    font-size: 24px;
    color: #000;
    font-family: "Roboto-Bold";
    align-self: center;
    text-align: center;
`;