import React, { useState, useEffect } from 'react';
import {
    Container,
    InputArea,
    CustomButton,
    CustomButtonText,
    CustomText,
    Span
} from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import SignInput from '../../components/SignInput';
import Api from '../../services/user-service';
import Dialog from "react-native-dialog";
import InviteFriend from '../../components/InviteFriend';
import { formatDate, parseDate, parseDateBR } from '../../helpers/date';
import { maskPhone } from '../../helpers/document';
import SignDatepicker from '../../components/SignDatepicker';

const EditAccount = ({ invite, setInvite }) => {


    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [main_phone, setMainPhone] = useState('');
    const [msg_phone, setMsgPhone] = useState('');
    const [birthdate, setBirthDate] = useState('');
    const [address, setAddress] = useState('');
    const navigation = useNavigation();
    const [visible, setVisible] = useState(false);

    const handleCancel = () => {
        setVisible(false);
    };

    const handleEdit = async () => {
        setVisible(false);

        if (name != '') {
            let json = await Api.updateUser(id, name, main_phone, msg_phone, parseDate(birthdate), address);
            if (json.name) {
                await AsyncStorage.setItem('name', json.name);
                alert("Dados atualizados com sucesso!")

                navigation.reset({
                    routes: [{ name: 'Minha Conta' }]
                });

            } else {
                alert("Erro: " + json.error);
            }
        }
    };

    useEffect(() => {
        const checkData = async () => {
            setId(await AsyncStorage.getItem('id'));
            let json = await Api.getUser(id);
            if (json.name) {
                setName(json.name);
                setMainPhone(json.main_phone);
                setMsgPhone(json.msg_phone);
                setBirthDate(parseDateBR(json.birthdate));
                setAddress(json.address);
            }
        }
        checkData();
    }, [id]);

    const handleClick = () => {
        setVisible(true);
    }

    return (
        <Container>
            <CustomText>Minha Conta - Editar</CustomText>
            <InputArea>
                <Span>Nome</Span>
                <SignInput
                    placeholder="Digite seu nome"
                    value={name}
                    onChangeText={t => setName(t)}
                    editable={true}
                />
                <Span>Telefone Principal</Span>
                <SignInput
                    placeholder="Digite seu telefone principal"
                    value={maskPhone(main_phone)}
                    numeric={true}
                    onChangeText={t => setMainPhone(t)}
                    editable={true}
                />
                <Span>Telefone de Recados</Span>
                <SignInput
                    placeholder="Digite seu telefone para recados"
                    value={maskPhone(msg_phone)}
                    numeric={true}
                    onChangeText={t => setMsgPhone(t)}
                    editable={true}
                />
                <Span>Data de Nascimento</Span>
                <SignDatepicker date={birthdate} setDate={setBirthDate} />
                <Span>Endereço</Span>
                <SignInput
                    placeholder="Digite seu endereço"
                    value={address}
                    onChangeText={t => setAddress(t)}
                    editable={true}
                />
            </InputArea>
            <CustomButton onPress={handleClick}>
                <CustomButtonText>Salvar</CustomButtonText>
            </CustomButton>
            <Dialog.Container visible={visible}>
                <Dialog.Title>Editar Conta</Dialog.Title>
                <Dialog.Description>
                    Você têm certeza que deseja atualizar os seus dados?
        </Dialog.Description>
                <Dialog.Button label="Cancelar" onPress={handleCancel} />
                <Dialog.Button label="Sim" onPress={handleEdit} />
            </Dialog.Container>
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

export default EditAccount;