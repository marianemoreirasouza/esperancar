import React, { useState, useEffect, useCallback } from 'react';
import {
  Container,
  CustomText,
  ScrollView,
  PartnersList,
  PartnersCards,
  CardTitle
} from './styles';
import InviteFriend from '../../components/InviteFriend';
import Api from '../../services/partner-service';
import { Text, Linking, Image } from 'react-native';
import { BASE_API } from "@env";

const Partners = ({ invite, setInvite }) => {

  const [partners, setPartners] = useState([]);

  const A = ({ url, children }) => {
    const handlePress = useCallback(async () => {
      const supported = await Linking.canOpenURL(url);

      if (supported) {
        await Linking.openURL(url);
      } else {
        Alert.alert(`A seguinte URL não pôde ser aberta: ${url}`);
      }
    }, [url]);

    return <Text style={{ fontWeight: 'bold', color: "#EF233C", textDecorationLine: 'underline' }} onPress={handlePress}>{children}</Text>;
  };


  useEffect(() => {
    const checkData = async () => {
      let json = await Api.getPartners();

      if (json.data) {
        setPartners(json.data);
      }
    }
    checkData();
  }, []);

  return (
    <Container>
      <CustomText>Parceiros</CustomText>
      <ScrollView>
        <PartnersList>
          {partners && partners.length > 0 ? partners.map((partner, k) => {
            return (
              <PartnersCards key={k}
                style={[{ borderColor: 'black' }]}
              >
                <Image style={{ height: 100 }} resizeMode={'contain'} source={{
                  uri: `${BASE_API}/storage/${partner.url_img}`
                }} />
                <CardTitle>{partner.name}</CardTitle>
                <A key={k} url={partner.url}>{partner.url}</A>
              </PartnersCards>
            )
          }) : null}
        </PartnersList>
      </ScrollView>
      <InviteFriend invite={invite} setInvite={setInvite} />
    </Container>
  );
};

export default Partners;