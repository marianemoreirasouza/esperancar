import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px;
`;

export const Image = styled.Image`
    width: 50%;
    height: 50%;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: #D90429;
    border-radius: 5px;
    justify-content: center;
    align-items: center;
    padding: 10px;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #FFF;
`;

export const Span = styled.Text`
    font-size: 14px;
    color: #D90429;
    font-weight: bold;
    margin-left: 10px;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;

export const AlertText = styled.Text`
    font-size: 18px;
    color: red;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;

export const InputArea = styled.View`
    width: 100%;
`;

export const Dropzone = styled.TouchableOpacity`
    width: 150px;
    height: 150px;
    border: 1px solid #D90429;
    border-radius: 10px;
    background-color: #f2f2f2;
`;

export const Column = styled.View`
    align-content: center;
    align-items: center;
    width: 50%;
`;

export const CenterContainer = styled.View`
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-content: center;
    margin-bottom: 50px;
`;