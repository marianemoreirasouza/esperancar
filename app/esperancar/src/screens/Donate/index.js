import React, { useState, useEffect } from 'react';
import { Container, CustomText, Span, CustomButton, CustomButtonText, InputArea, Dropzone, CenterContainer, Column, AlertText } from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import InviteFriend from '../../components/InviteFriend';
import SignInput from '../../components/SignInput';
import ButtonIcon from '../../components/ButtonIcon';
import { Image } from 'react-native'
import * as ImagePicker from 'react-native-image-picker';
import { faImage, faCamera } from '@fortawesome/free-solid-svg-icons';
import Dialog from "react-native-dialog";
import { useNavigation } from '@react-navigation/native';
import Api from '../../services/donation-service';
import SignDatepicker from '../../components/SignDatepicker';
import { parseDate } from '../../helpers/date';

const Donate = ({ invite, setInvite }) => {

    const [userId, setUserId] = useState('');
    const [date, setDate] = useState('');
    const [qty, setQty] = useState('');
    const [photo, setPhoto] = useState(null);
    const [visible, setVisible] = useState(false);
    const [nextDonation, setNextDonation] = useState(0);
    const navigation = useNavigation();

    useEffect(() => {

        const checkData = async () => {
            let last_donation = new Date(await AsyncStorage.getItem('last_donation'));
            let sex = new Date(await AsyncStorage.getItem('sex'));
            canDonate(last_donation, sex);
        }
        checkData();
    }, [userId]);

    const canDonate = (last_donation, sex) => {

        let date = new Date();
        let diffDays = Math.ceil(Math.abs(last_donation - date) / (1000 * 60 * 60 * 24)) - 1

        if (sex == 'F')
            setNextDonation(90 - diffDays);
        else
            setNextDonation(60 - diffDays);
    }

    const handleChoosePhoto = () => {
        const options = {
            mediaType: 'photo',
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response.uri) {
                setPhoto(response);
            }
        });
    }

    const handleTakePhoto = () => {
        const options = {
            mediaType: 'photo',
            noData: true,
        }
        ImagePicker.launchCamera(options, response => {
            if (response.uri) {
                setPhoto(response);
            }
        })
    }

    const handleClick = () => {
        if (date != '' && qty != '' && photo != null) {
            setVisible(true);
        } else {
            alert("Preencha todos os campos!");
        }
    }

    const handleCancel = () => {
        setVisible(false);
    };

    const createFormData = (photo, body) => {
        const data = new FormData();

        data.append("photo", {
            name: photo.fileName,
            type: photo.type,
            size: photo.fileSize,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });

        return data;
    };

    const submit = async () => {
        setVisible(false);

        if (photo != null && date != '' && qty != '') {
            let json = await Api.postDonation(createFormData(photo, { 'user_id': userId, 'date': parseDate(date), 'qty_bags': qty }));
            if (json != 'error') {
                navigation.reset({
                    routes: [{ name: 'Minhas Doações' }]
                });
                alert("Doação informada com sucesso!");
            }
            else if (json == 'error') {
                alert("Erro! Não existe uma ação para a data informada!");
            } else {
                alert("Aconteceu um erro, tente novamente!");
            }
        } else {
            alert("Todos os campos são obrigatórios!");
        }

    };

    useEffect(() => {
        const checkData = async () => {
            if (userId == '') setUserId(await AsyncStorage.getItem('id'));
        }
        checkData();
    }, []);


    return (
        <Container>
            <CustomText>Informar Doação</CustomText>
            {nextDonation > 0 &&
                <AlertText>Você precisa aguardar mais {nextDonation} dias antes de fazer sua doação </AlertText>
            }

            <InputArea>
                <Span>Data</Span>
                <SignDatepicker date={date} setDate={setDate} disabled={nextDonation > 0} />
                <Span>Quantidade de bolsas</Span>
                <SignInput
                    placeholder="Digite a quantidade"
                    value={qty}
                    onChangeText={t => setQty(t)}
                    numeric={true}
                    editable={nextDonation <= 0}
                />
                <Span>Foto</Span>
                <CenterContainer>
                    <Column>
                        {photo ?
                            <Image
                                source={{ uri: photo.uri }}
                                style={{ width: 150, height: 150, borderRadius: 10, marginLeft: 15 }}
                            />
                            :
                            <Dropzone />
                        }
                    </Column>
                    <Column>
                        <ButtonIcon onPress={handleChoosePhoto} icon={faImage} disabled={nextDonation > 0} />
                        <ButtonIcon onPress={handleTakePhoto} icon={faCamera} disabled={nextDonation > 0} />
                    </Column>
                </CenterContainer>
                <CustomButton onPress={handleClick} disabled={nextDonation > 0}>
                    <CustomButtonText>Salvar</CustomButtonText>
                </CustomButton>
                <Dialog.Container visible={visible}>
                    <Dialog.Title>Informar doação</Dialog.Title>
                    <Dialog.Description>
                        Você têm certeza que deseja informar doação?
        </Dialog.Description>
                    <Dialog.Button label="Cancelar" onPress={handleCancel} />
                    <Dialog.Button label="Sim" onPress={submit} />
                </Dialog.Container>
            </InputArea>
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

export default Donate;