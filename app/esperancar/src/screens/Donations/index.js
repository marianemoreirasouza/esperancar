import React, { useState, useEffect } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Container, CustomText, Icon, ScrollView } from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import InviteFriend from '../../components/InviteFriend';
import ShareDonation from '../../components/ShareDonation';
import Api from '../../services/donation-service';
import { Table, Row, Rows } from 'react-native-table-component';
import { parseDateMonthBR, parseDateYearBR } from '../../helpers/date';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faShareAltSquare } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import RoundButton from '../../components/RoundButton';
import { Dimensions } from "react-native";

const Donations = ({ invite, setInvite }) => {

    const [userId, setUserId] = useState('');
    const [donations, setDonations] = useState([]);
    const [donationShared, setDonationShared] = useState({ bags: '', date: '', file: '' });
    const [share, setShare] = useState(false);
    const tableHead = ['#', 'Mês', 'Ano', 'Bolsas', 'Ações'];
    const widthArr = [40, 100, 50, 60, 60];

    useEffect(() => {
        const checkData = async () => {
            if (userId == '') setUserId(await AsyncStorage.getItem('id'));

            let response = await Api.getDonations(userId);
            if (response.data && donations.length == 0) {
                const tableData = [];
                let data = response.data;
                data.map((donation, k) => {
                    tableData.push([k + 1,
                    parseDateMonthBR(donation.date),
                    parseDateYearBR(donation.date),
                    donation.qty_bags,
                    <Icon>
                        <FontAwesomeIcon style={{ color: '#D90429' }} icon={faShareAltSquare} size={25} onPress={() => {
                            setShare(true);
                            setDonationShared(donation);
                        }} />
                    </Icon>
                    ]);
                });

                setDonations(tableData);
            }
        }
        checkData();
    }, [donations]);


    return (
        <Container>
            <CustomText>Minhas Doações</CustomText>
            <ScrollView>
                {donations.length > 0 ?
                    <Table borderStyle={{ borderWidth: 2, borderColor: '#000' }}>
                        <Row data={tableHead} widthArr={widthArr} style={styles.head} textStyle={styles.headText} />
                        <Rows data={donations} widthArr={widthArr} textStyle={styles.text} />
                    </Table>
                    :
                    <Text style={styles.noDonation}>Você ainda não fez doações!</Text>
                }
            </ScrollView>
            <RoundButton icon={faPlus} screen={"Doar"} ht={Dimensions.get("window").height * 0.025} />
            <ShareDonation share={share} setShare={setShare} donation={donationShared} />
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

const styles = StyleSheet.create({
    head: { height: 40, backgroundColor: '#2b2d42' },
    headText: { color: "#fff", textAlign: 'center', fontWeight: 'bold' },
    text: { margin: 6, textAlign: 'center' },
    noDonation: { fontSize: 20, color: 'red', marginTop: 40}
});


export default Donations;