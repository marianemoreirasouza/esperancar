import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: #fff;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px;
`;

export const Icon = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
    align-content: center;
`;

export const ScrollView = styled.ScrollView`
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    width: 100px;
    background-color: #D90429;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    margin-top: 20px;
`;

export const CustomButtonText = styled.Text`
    color: #FFF;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;