import React, { useState, useEffect } from 'react';
import {
    Container,
    CustomText,
} from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import InviteFriend from '../../components/InviteFriend';
import NotificationCards from '../../components/NotificationCards';
import Api from '../../services/alert-service';

const Notifications = ({ invite, setInvite }) => {

    const [id, setId] = useState('');
    const [alerts, setAlerts] = useState([]);
    const [update, setUpdate] = useState(false);

    useEffect(() => {
        const checkData = async () => {
            setId(await AsyncStorage.getItem('id'));

            let json = await Api.getAlerts(id);
            if (json.data) {
                setAlerts(json.data)
            }
        }
        const intervalId = setInterval(() => {    
            checkData();
        }, 3000);
        checkData();
        return () => clearInterval(intervalId);
    }, [update, id, useState]);

    return (
        <Container>
            <CustomText>Notificações</CustomText>
            <NotificationCards alerts={alerts} setAlerts={setAlerts} update={update} setUpdate={setUpdate} />
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

export default Notifications;