import React, { useEffect } from 'react';
import { Container, Image, LoadingIcon } from './styles';
import LogoEsperancar from '../../assets/logo-sm.png';
import AsyncStorage from '@react-native-community/async-storage';

const Preload = ({ token, ...props }) => {

    useEffect(() => {
        if (token !== null) {
            props.navigation.navigate('Dashboard');
        } else {
            props.navigation.navigate('Entrar');
        }

    }, [token]);

    return (
        <Container>
            <Image source={LogoEsperancar} />
            <LoadingIcon size="large" color="red" />
        </Container>
    );


}

export default Preload;