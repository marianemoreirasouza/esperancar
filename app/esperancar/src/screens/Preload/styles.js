import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: white;
    flex: 1;
    justify-content: center;
    align-items: center;
    
    `;

export const Image = styled.Image`
    width: 50%;
    height: 50%;
    `;

export const LoadingIcon = styled.ActivityIndicator`
    margin-top: 50px;
`;