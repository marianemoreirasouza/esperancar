import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    background-color: #fff;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 0px 40px;
`;

export const InputArea = styled.View`
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: #D90429;
    border-radius: 5px;
    justify-content: center;
    align-items: center;
    padding: 10px;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #FFF;
`;

export const CustomText = styled.Text`
    font-size: 32px;
    color: #000;
    font-weight: bold;
    margin-top: 20px;
    margin-bottom: 15px;
    align-self: flex-start;
`;

export const Span = styled.Text`
    color: #EF233C;
    font-weight: bold;
    font-size: 18px;
    font-family: "Roboto-Bold";
    align-self: flex-start;
    text-align: left;
`;

export const Column = styled.View`
    justify-content: center;
    align-content: center;
    align-items: center;
    width: 50%;
`;

export const CenterContainer = styled.View`
    flex: 1;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    padding: 20px;
`;

export const SwitchButton = styled.Switch`
    align-self: flex-end;
`;

export const Count = styled.Text`
    font-size: 24px;
    color: #000;
    font-family: "Roboto-Bold";
    align-self: center;
    text-align: center;
`;