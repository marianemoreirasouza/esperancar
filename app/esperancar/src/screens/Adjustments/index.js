import React, { useState, useEffect } from 'react';
import {
    Container,
    InputArea,
    CustomText,
    SwitchButton,
    Span,
    Column,
    CenterContainer
} from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import InviteFriend from '../../components/InviteFriend';
import Api from '../../services/user-service';

const Adjustments = ({ invite, setInvite }) => {

    const [id, setId] = useState('');
    const [alerts, setAlerts] = useState('');

    useEffect(() => {
        const checkData = async () => {
            setId(await AsyncStorage.getItem('id'));
            setAlerts(await AsyncStorage.getItem('alerts'));
        }
        checkData();
    }, []);

    const handleEdit = async () => {
        let is_enabled = (alerts == 1 ? 0 : 1);
        setAlerts(is_enabled);

        let json = await Api.updateAlerts(id, is_enabled);

        if (json.name) {
            await AsyncStorage.setItem('alerts', json.alerts.toString());
            alert("Ajuste de notificação atualizado com sucesso!")
        } else {
            alert("Erro: " + json.error);
        }

    };

    return (
        <Container>
            <CustomText>Ajustes</CustomText>
            <CenterContainer>
                <Column><Span>Notificações</Span></Column>
                <Column>
                    <SwitchButton
                        trackColor={{ false: "lightgrey", true: "lightgrey" }}
                        thumbColor={alerts ? "#D90429" : "#f4f3f4"}
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={handleEdit}
                        value={alerts == 1 ? true : false}
                    />
                </Column>
            </CenterContainer>
            <InviteFriend invite={invite} setInvite={setInvite} />
        </Container>
    );
};

export default Adjustments;