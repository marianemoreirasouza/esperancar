
import React, { useState, useEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AsyncStorage from '@react-native-community/async-storage';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import Dashboard from '../screens/Dashboard';
import Account from '../screens/Account';
import EditAccount from '../screens/EditAccount';
import CustomDrawer from '../components/CustomDrawer';
import Preload from '../screens/Preload';
import Adjustments from '../screens/Adjustments';
import Notifications from '../screens/Notifications';
import CustomHeader from '../components/CustomHeader';
import Donations from '../screens/Donations';
import Donate from '../screens/Donate';
import FAQ from '../screens/Faq';
import Partners from '../screens/Partners';

const Drawer = createDrawerNavigator();

export default () => {

    const [invite, setInvite] = useState(false);
    const [token, setToken] = useState(null);

    useEffect(() => {
        const checkToken = async () => {
            setToken(await AsyncStorage.getItem('token'));
        }
        checkToken();
    }, [token]);

    return (
        <Drawer.Navigator
            initialRouteName="Preload"
            screenOptions={{
                headerShown: true,
                gestureEnabled: true,
                headerTintColor: 'white',
                headerTitle: props => <CustomHeader {...props} style={{color: 'white'}} token={token} />,
                headerStyle: {
                    float: 'right',
                    backgroundColor: '#2b2d42',
                    tintColor: 'white',
                    color: 'white'
                }
            }}
            drawerPosition={'left'}
            drawerContent={props => <CustomDrawer {...props} setInvite={setInvite} token={token} setToken={setToken} />}
        >

            <Drawer.Screen name="Preload" children={(props) => <Preload {...props} token={token} />} />
            <Drawer.Screen name="Dashboard" children={() => <Dashboard invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Minha Conta" children={() => <Account invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Editar Conta" children={() => <EditAccount invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Entrar" children={() => <SignIn invite={invite} setInvite={setInvite} token={token} setToken={setToken} />} />
            <Drawer.Screen name="Seja um Doador" children={() => <SignUp invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Ajustes" children={() => <Adjustments invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Notificações" children={() => <Notifications invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Minhas Doações" children={() => <Donations invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Doar" children={() => <Donate invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="FAQ" children={() => <FAQ invite={invite} setInvite={setInvite} />} />
            <Drawer.Screen name="Parceiros" children={() => <Partners invite={invite} setInvite={setInvite} />} />

        </Drawer.Navigator>
    );
};