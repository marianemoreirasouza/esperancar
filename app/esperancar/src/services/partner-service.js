import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {

    getPartners: async () => {
        let url = BASE_API;
        const req = await fetch(`${url}/api/partners-data`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            }
        });
        const json = await req.json();
        return json;
    }

};