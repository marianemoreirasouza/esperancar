import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {

    getAlerts: async (user_id) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/alerts?user_id=${user_id}&embed=informs`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const json = await req.json();
        return json;
    },

    getAlertsUnread: async (user_id) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/alerts?user_id=${user_id}&is_read=0&embed=informs`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const json = await req.json();
        return json;
    },

    setAlertRead: async (id, is_read) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/alerts/${id}`, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            },
            body: JSON.stringify({ is_read })
        });
        const json = await req.json();
        return json;
    }

};