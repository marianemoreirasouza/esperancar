import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {

    getFaq: async () => {
        let url = BASE_API;
        const req = await fetch(`${url}/api/faq-data`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        });
        const json = await req.json();
        return json;
    }

};