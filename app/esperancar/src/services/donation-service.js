import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {

    getDonations: async (user_id) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/donations?embed=acao,arquivo&user_id=${user_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const json = await req.json();
        return json;
    },
    getDonationsData: async (user_id) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/donations-data?user_id=${user_id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        });
        const json = await req.json();
        return json;
    },
    postDonation: async (data) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = fetch(`${url}/api/donations`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token,
            },
            body: data
        }).then(response => response.json())
            .then(response => {
                // console.log("upload success", response);
                if(response != 'error')
                    return true;
                else 
                    return response;
            })
            .catch(error => {
                // console.log("upload error", error);
                return false;
            });
        return await req;
    }

};