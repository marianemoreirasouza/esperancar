import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {
    signIn: async (email, password) => {
        let url = BASE_API;
        const req = await fetch(`${url}/api/auth/login`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email, password})
        });
        const json = await req.json();   
        return json;
    },
    signUp: async (name, cpf, sex, main_phone, msg_phone, blood_type, email, birthdate, address, password) => {
        let url = BASE_API;
        const req = await fetch(`${url}/api/users-create`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({name, cpf, sex, main_phone, msg_phone, blood_type, email, birthdate, address, password})
        });
        const json = await req.json();  
        return json;
    }

};