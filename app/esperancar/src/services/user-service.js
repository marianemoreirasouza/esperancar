import AsyncStorage from '@react-native-community/async-storage';
import {BASE_API} from "@env";

export default {
    getUser: async (id) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/users/${id}`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ token,
            }
        });
        const json = await req.json();   
        return json.data;
    },

    updateUser: async (id, name, main_phone, msg_phone, birthdate, address) => {
        let url = BASE_API;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/users/${id}`, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ token,
            },
            body: JSON.stringify({name, main_phone, msg_phone, birthdate, address})
        });
        const json = await req.json();  
        return json;
    },

    updateAlerts: async (id, alerts) => {
        let url = BASE_API;
        let enabled = alerts;
        const token = await AsyncStorage.getItem('token');
        const req = await fetch(`${url}/api/users/${id}`, {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ token,
            },
            body: JSON.stringify({alerts})
        });
        const json = await req.json();  
        return json;
    }

};