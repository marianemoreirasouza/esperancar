import React from "react";
import styled from "styled-components";
import LottieView from 'lottie-react-native';

const Drop = styled.View`
    height: 115px;
    width: 115px;
    border-radius: 100px;
    border-top-right-radius: 0;
    transform: rotate(-45deg);
    position: absolute;
    background: #2b2d42;
    border: 2px solid black;
    z-index: 0;
    align-content: center;
    justify-content: center;
    align-items: center;
`;

const Type = styled.Text`
    transform: rotate(45deg);
    font-size: 36px;
    color: #fff;
    font-weight: bold;
    position: absolute;
    padding-top: 45px;
`;

export default ({ blood_type }) => {
    return (
        <>
            <Drop>
                <LottieView style={{
                    position: 'absolute',
                    width: 152,
                    transform: [{ rotate: '25deg' }]
                }} source={require('../assets/wave3.json')} autoPlay loop />
                <Type>{blood_type}</Type>
            </Drop>
        </>
    );
}