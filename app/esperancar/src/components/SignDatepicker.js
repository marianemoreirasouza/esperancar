import React from 'react';
import styled from 'styled-components/native';
import DatePicker from 'react-native-datepicker'

const InputArea = styled.View`
    width: 100%;
    height: 60px;
    background-color: #fff;
    flex-direction: row;
    border-radius: 30px;
    align-items: center;
    margin-bottom: 15px;
`;


export default ({ date, setDate, disabled }) => {
    return (
        <InputArea>
            <DatePicker
                style={{ flex: 1}}
                date={date}
                mode="date"
                placeholder="Selecione a data"
                format="DD/MM/YYYY"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                showIcon={false}
                disabled={disabled}
                customStyles={{
                    dateInput: {
                        borderRadius: 10,
                        height: 60,
                        borderColor: 'red',
                        fontSize: '16px',
                        alignItems: 'flex-start',
                        paddingLeft: 15
                    },
                    placeholderText: {
                        fontSize: 16,
                        color: '#000'
                    }
                }}
                onDateChange={(d) => { setDate(d) }}
            />
        </InputArea >
    );
}