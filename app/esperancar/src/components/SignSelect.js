import React from 'react';
import styled from 'styled-components/native';

const InputArea = styled.View`
    width: 100%;
    height: 60px;
    background-color: #fff;
    flex-direction: row;
    margin-bottom: 15px;
    border: 1px solid red;
    border-radius: 10px;
`;
const Picker = styled.Picker`
    flex: 1;
    font-size: 16px;
    color: #000;
    padding: 30px;
    margin-left: 15px;
    justify-content: center;
`;

export default ({ value, setValue, options }) => {
    return (
        <InputArea>
            <Picker
                selectedValue={value}
                onValueChange={t => setValue(t)}
            >
                {options.map((item, k) => (
                    <Picker.Item key={k} label={item.label} value={item.value} />
                ))}
            </Picker>
        </InputArea>
    );
}