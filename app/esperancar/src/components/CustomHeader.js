import React, { useEffect, useState } from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faBell } from '@fortawesome/free-solid-svg-icons';
import AsyncStorage from '@react-native-community/async-storage';
import Api from '../services/alert-service';

const CustomHeader = styled.View`
    flex: 1;
    align-items: flex-end
`;

const Count = styled.Text`
    position: absolute;
    top: -5px;
    right: -10px;
    padding: 1px 6px;
    border-radius: 50px;
    background: red;
    color: white;
    font-weight: bold;
`;

export default ({ token, ...props }) => {
    const navigation = useNavigation();
    const [id, setId] = useState('');
    const [alerts, setAlerts] = useState([]);

    useEffect(() => {
        const checkData = async () => {
            setId(await AsyncStorage.getItem('id'));
            if (id != null) {
                let json = await Api.getAlertsUnread(id);
                if (json.data) {
                    setAlerts(json.data.length)
                }
            }
        }
        checkData();

        const intervalId = setInterval(() => {
            checkData();
        }, 5000);
        return () => clearInterval(intervalId);
    }, [token, id, useState]);

    return (
        <CustomHeader {...props}>
            {token &&
                <>
                    <FontAwesomeIcon style={{ color: '#FFE66D' }} icon={faBell} size={25} onPress={() => navigation.navigate('Notificações')} />
                    {alerts > 0 &&
                        <Count onPress={() => navigation.navigate('Notificações')}>{alerts}</Count>
                    }
                </>
            }
        </CustomHeader>
    );
}