import React, { useState } from 'react';
import styled from 'styled-components/native';
import { StyleSheet, Modal, View, Text, Image, TouchableOpacity } from 'react-native';
import Api from '../services/alert-service';
import { BASE_API } from "@env";

const List = styled.ScrollView`
`;

const NotificationList = styled.View`
    flex: 1;
    justify-content: space-around;
    flex-wrap: wrap;
    padding: 0px 25px;
`;

const NotificationCards = styled.TouchableOpacity`
    margin: 10px;
    padding: 30px;
    height: 110px;
    border-radius: 10px;
    border: 1px solid black;
`;

const CardTitle = styled.Text`
    text-align: left;
    font-weight: bold;
    font-size: 20px;
    margin-top: -10px;
    height: 40px;
`;

const Title = styled.Text`
    text-align: left;
    font-weight: bold;
    font-size: 20px;
    height: 40px;
`;

const CardText = styled.Text`
    text-align: left;
    font-size: 16px;
    margin-top: -10px;
    height: 40px;
`;

const ModalCardText = styled.Text`
    text-align: left;
    font-size: 16px;
    margin-top: -10px;
`;

const DotIcon = styled.Text`
    position: absolute;
    top: 60%;
    left: 110%;
`;

export default ({ alerts, setAlerts, update, setUpdate }) => {
    const [showNotification, SetShowNotification] = useState(false);
    const [notification, SetNotification] = useState({});

    const updateNotificationRead = async (al) => {

        let response = await Api.setAlertRead(al.id, 1);
        if (response) {
            setUpdate(!update);
        }
    }

    return (
        <>
            <List>
                <NotificationList>
                    {alerts && alerts.length > 0 ? alerts.map((al, k) => {
                        return (
                            <NotificationCards key={k} onPress={() => {
                                SetShowNotification(true);
                                SetNotification(al);
                                updateNotificationRead(al);
                            }}
                                style={[{ borderColor: al.is_read == 1 ? 'black' : 'red' }]}
                            >
                                <CardTitle>{al.title}</CardTitle>
                                <CardText>{al.text}</CardText>
                                <DotIcon style={[{ color: al.is_read == 1 ? '#bbb' : 'red' }]}>{'\u2B24'}</DotIcon>
                            </NotificationCards>
                        )
                    }) : <Title>Você não possui nenhuma notificação!</Title>}
                </NotificationList>
            </List>
            <Modal
                animationType="fade"
                transparent={true}
                visible={showNotification}
                onRequestClose={() => {
                    SetShowNotification(false);
                    SetNotification({})
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <TouchableOpacity
                            style={{ backgroundColor: "#fff" }}
                            onPress={() => {
                                SetShowNotification(!showNotification);
                            }}
                        >
                            <Text style={styles.textStyle}>X</Text>
                        </TouchableOpacity>
                        <CardTitle>{notification.title}</CardTitle>
                        <ModalCardText>{notification.text}</ModalCardText>
                        {notification.informs &&
                            <Image style={{ height: 150, marginTop: 30 }} resizeMode={'contain'} source={{
                                uri: `${BASE_API}/storage/${notification.informs.url_img}`
                            }}
                            />
                        }
                    </View>
                </View>
            </Modal>
        </>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(100,100,100, 0.5)',
    },
    modalView: {
        margin: 20,
        width: '80%',
        alignSelf: 'center',
        backgroundColor: "white",
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "black",
        fontWeight: "bold",
        textAlign: "right",
        fontSize: 18
    },
    modalText: {
        marginBottom: 15,
        textAlign: "right"
    }
});