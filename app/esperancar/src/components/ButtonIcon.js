import React from 'react';
import styled from 'styled-components/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'



const ButtonIcon = styled.TouchableOpacity`
    width: 50px;
    height: 50px;
    background-color: #D90429;
    border-radius: 10px;
    justify-content: center;
    align-items: center;
    margin: 10px;
`;


export default ({icon, ...props}) => {
    return (
        <ButtonIcon {...props}>
            <FontAwesomeIcon style={{ color: 'white' }} icon={icon} size={25} />
        </ButtonIcon>
    );
}