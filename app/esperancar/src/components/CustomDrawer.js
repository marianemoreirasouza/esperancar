
import React, { useEffect, useState } from 'react';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-community/async-storage';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faUser, faCog, faShareAlt, faSignOutAlt, faSignInAlt, faTint, faQuestion, faBriefcase } from '@fortawesome/free-solid-svg-icons';
import { CommonActions } from '@react-navigation/native';

export default ({ setInvite, token, setToken, ...props }) => {
    useEffect(() => {
    }, [props.navigation, token]);

    const getActiveRouteState = function (routes, index, name) {
        return routes[index].name.toLowerCase().indexOf(name.toLowerCase()) >= 0;
    };

    const inactivatedColor = 'white';
    const activatedColor = '#2B2D42';

    const opt = {
        activeBackgroundColor: '#2B2D42',
        activeTintColor: 'white',
        labelStyle: {
            fontSize: 16
        }
    }

    return (
        <DrawerContentScrollView {...props}>
            {token != null ? (<>
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Dashboard'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faHome} size={20} />}
                    label="Início"
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Dashboard'
                    )}
                    onPress={() => props.navigation.navigate('Dashboard')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Minhas Doações'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faTint} size={20} />}
                    label="Minhas Doações"
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Minhas Doações'
                    )}
                    onPress={() => props.navigation.navigate('Minhas Doações')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Minha Conta'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faUser} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Minha Conta'
                    )}
                    label="Minha Conta"
                    onPress={() => props.navigation.navigate('Minha Conta')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'FAQ'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faQuestion} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'FAQ'
                    )}
                    label="FAQ"
                    onPress={() => props.navigation.navigate('FAQ')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Parceiros'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faBriefcase} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Parceiros'
                    )}
                    label="Parceiros"
                    onPress={() => props.navigation.navigate('Parceiros')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Ajustes'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faCog} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Ajustes'
                    )}
                    label="Ajustes"
                    onPress={() => props.navigation.navigate('Ajustes')
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Convidar amigo(a)'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faShareAlt} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Convidar amigo(a)'
                    )}
                    label="Convidar amigo(a)"
                    onPress={() => {
                        props.navigation.closeDrawer()
                        setInvite(true)
                    }
                    }
                />
                <DrawerItem
                    {...opt}
                    icon={() => <FontAwesomeIcon style={
                        {
                            color: getActiveRouteState(
                                props.state.routes,
                                props.state.index,
                                'Sair'
                            ) ? inactivatedColor : activatedColor
                        }
                    }
                        icon={faSignOutAlt} size={20} />}
                    focused={getActiveRouteState(
                        props.state.routes,
                        props.state.index,
                        'Sair'
                    )}
                    label="Sair"
                    onPress={() => {
                        AsyncStorage.clear().then(response => {
                            props.navigation.dispatch(
                                CommonActions.reset({
                                    index: 0,
                                    routes: [
                                        { name: 'Entrar' }
                                    ],
                                })
                            );
                            setToken(null);
                        }).done();
                        props.navigation.navigate('Entrar');
                    }
                    }
                />
            </>)
                : (<>
                    <DrawerItem
                        {...opt}
                        icon={() => <FontAwesomeIcon style={
                            {
                                color: getActiveRouteState(
                                    props.state.routes,
                                    props.state.index,
                                    'Entrar'
                                ) ? inactivatedColor : activatedColor
                            }
                        }
                            icon={faSignInAlt} size={20} />}
                        focused={getActiveRouteState(
                            props.state.routes,
                            props.state.index,
                            'Entrar'
                        )}
                        label="Entrar"
                        onPress={() => props.navigation.navigate('Entrar')
                        }
                    />
                    <DrawerItem
                        {...opt}
                        icon={() => <FontAwesomeIcon style={
                            {
                                color: getActiveRouteState(
                                    props.state.routes,
                                    props.state.index,
                                    'Seja um Doador'
                                ) ? inactivatedColor : activatedColor
                            }
                        }
                            icon={faTint} size={20} />}
                        focused={getActiveRouteState(
                            props.state.routes,
                            props.state.index,
                            'Seja um Doador'
                        )}
                        label="Seja um Doador"
                        onPress={() => props.navigation.navigate('Seja um Doador')
                        }
                    />
                    <DrawerItem
                        {...opt}
                        icon={() => <FontAwesomeIcon style={
                            {
                                color: getActiveRouteState(
                                    props.state.routes,
                                    props.state.index,
                                    'FAQ'
                                ) ? inactivatedColor : activatedColor
                            }
                        }
                            icon={faQuestion} size={20} />}
                        focused={getActiveRouteState(
                            props.state.routes,
                            props.state.index,
                            'FAQ'
                        )}
                        label="FAQ"
                        onPress={() => props.navigation.navigate('FAQ')
                        }
                    />
                    <DrawerItem
                        {...opt}
                        icon={() => <FontAwesomeIcon style={
                            {
                                color: getActiveRouteState(
                                    props.state.routes,
                                    props.state.index,
                                    'Parceiros'
                                ) ? inactivatedColor : activatedColor
                            }
                        }
                            icon={faBriefcase} size={20} />}
                        focused={getActiveRouteState(
                            props.state.routes,
                            props.state.index,
                            'Parceiros'
                        )}
                        label="Parceiros"
                        onPress={() => props.navigation.navigate('Parceiros')
                        }
                    />
                    <DrawerItem
                        {...opt}
                        icon={() => <FontAwesomeIcon style={
                            {
                                color: getActiveRouteState(
                                    props.state.routes,
                                    props.state.index,
                                    'Convidar amigo(a)'
                                ) ? inactivatedColor : activatedColor
                            }
                        }
                            icon={faShareAlt} size={20} />}
                        focused={getActiveRouteState(
                            props.state.routes,
                            props.state.index,
                            'Convidar amigo(a)'
                        )}
                        label="Convidar amigo(a)"
                        onPress={() => {
                            props.navigation.closeDrawer()
                            setInvite(true)
                        }
                        }
                    />
                </>)
            }
        </DrawerContentScrollView>
    );
}