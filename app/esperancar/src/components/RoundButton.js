import React from 'react';
import styled from 'styled-components/native'
import { StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'


const RoundButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    position: absolute;
    left: 90%;
    width: 80px;
    height: 80px;
    border-radius: 100px;
    background-color: #D90429;
`;

export default ({ icon, screen, ht }) => {
    const navigation = useNavigation();

    const styles = StyleSheet.create({
        round: { bottom: ht }
    });

    return (
        <RoundButton style={styles.round}
            onPress={() => navigation.navigate(screen)}>
                <FontAwesomeIcon style={{ color: 'white' }} icon={icon} size={25} />
        </RoundButton>
    );
};


