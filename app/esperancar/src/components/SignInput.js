import React from 'react';
import styled from 'styled-components/native';

const InputArea = styled.View`
    width: 100%;
    height: 60px;
    background-color: #fff;
    flex-direction: row;
    border-radius: 30px;
    align-items: center;
    margin-bottom: 15px;
`;
const Input = styled.TextInput`
    flex: 1;
    font-size: 16px;
    color: #000;
    border: 1px solid #D90429;
    border-radius: 10px;
    padding: 20px;
    justify-content: center;
    font-family: "Roboto-Regular";
`;

export default ({placeholder, numeric, value, onChangeText, password, editable}) => {
    return (
        <InputArea>
            <Input
                placeholder={placeholder}
                placeholderTextColor="#000"
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={(value.length > 0 && password) ? true : false}
                editable={editable}
                keyboardType={numeric ? 'numeric': 'default'}
                backgroundColor={editable ? "#fff" : "#f2f2f2"}
            />
        </InputArea>
    );
}