import React from 'react';
import { StyleSheet, Modal, View, Button, Text, TouchableOpacity } from 'react-native';
import Share from 'react-native-share';
import { APP_ANDROID, APP_IOS } from "@env";

const InviteFriend = ({ invite, setInvite }) => {
  const url = Platform.OS === "android" ? APP_ANDROID : APP_IOS;
  const onShare = async () => {
    try {
      Share.open({
        title: 'Esperançar',
        subject: 'Seja um doador!',
        url: url,
        message:
          `Seja um doador de sangue e salve vidas! Baixe o app Esperançar`,
      }).then((res) => {
        console.log(res);
      })
        .catch((err) => {
          err && console.log(err);
        });
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={invite}
        onRequestClose={() => {
          setInvite(false)
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              style={{ backgroundColor: "#fff" }}
              onPress={() => {
                setInvite(!invite);
              }}
            >
              <Text style={styles.textStyle}>X</Text>
            </TouchableOpacity>
            <View style={styles.content}>
              <Text style={{
                textAlign: 'center',
                fontWeight: 'bold',
                height: 50
              }}>Convide um(a) amigo(a) para ser um(a) doador(a) e ajude-nos a salvar vidas!</Text>
              <TouchableOpacity style={styles.buttonShare} onPress={onShare}>
                <Text style={styles.textButtonShare}>Convidar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View >
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(100,100,100, 0.5)',
  },
  modalView: {
    margin: 50,
    width: 'auto',
    alignSelf: 'center',
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "right",
    fontSize: 18
  },
  modalText: {
    marginBottom: 15,
    textAlign: "right"
  },
  buttonShare: {
    height: 40,
    width: 150,
    backgroundColor: "#D90429",
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  textButtonShare: {
    fontSize: 14,
    color: "#FFF",
    fontFamily: "Roboto-Bold"
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  }
});

export default InviteFriend;