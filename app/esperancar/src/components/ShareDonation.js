import React, { useState } from 'react';
import { StyleSheet, Modal, TouchableHighlight, View, TouchableOpacity, Text } from 'react-native';
import { parseDateBR } from '../helpers/date';
import Share from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';
import { BASE_API } from "@env";

const ShareDonation = ({ share, setShare, donation }) => {

  const [url, setUrl] = useState('');

  const onShare = async () => {
    try {
      RNFetchBlob.fetch('GET', `${BASE_API}/storage/${donation.arquivo.storage_name}`)
        .then(resp => {
          var base64image = resp.data;
          setUrl('data:image/png;base64,' + base64image);
          let base = base64image;
          Share.open({
            title: 'Esperançar',
            subject: 'Doação',
            url: 'data:image/png;base64,' + base64image,
            message:
              `Fiz uma doação de ${donation.qty_bags} bolsa(s) de sangue em ${parseDateBR(donation.date)} e salvei vidas!`,
          }).then((res) => {
            console.log(res);
          })
            .catch((err) => {
              err && console.log(err);
            });
        })
        .catch(err => errorHandler(err));
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={share}
        onRequestClose={() => {
          setShare(false)
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              style={{ backgroundColor: "#fff" }}
              onPress={() => {
                setShare(!share);
              }}
            >
              <Text style={styles.textStyle}>X</Text>
            </TouchableOpacity>
            <Text style={{
              textAlign: 'center',
              fontWeight: 'bold',
              height: 50
            }}>Compartilhar minha doação</Text>
            <TouchableOpacity style={styles.buttonShare} onPress={onShare}>
              <Text style={styles.textButtonShare}>Compartilhar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(100,100,100, 0.5)',
  },
  modalView: {
    margin: 50,
    width: 'auto',
    alignSelf: 'center',
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "right",
    fontSize: 18
  },
  modalText: {
    marginBottom: 15,
    textAlign: "right"
  },
  buttonShare: {
    height: 40,
    width: 150,
    backgroundColor: "#D90429",
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center'
  },
  textButtonShare: {
    fontSize: 14,
    color: "#FFF",
    fontFamily: "Roboto-Bold"
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  }
});

export default ShareDonation;