import axios from 'axios';
import store from 'store';
import i18n from 'i18next';
import { toast } from 'react-toastify';
import swal from '../helpers/swal';

// const api = axios.create({
//   baseURL: 'http://esperancar.test/api',
// });
const api = axios.create({ baseURL: process.env.REACT_APP_API_URL });

api.interceptors.request.use((config) => {
  if (!!localStorage.token)
    config.headers.Authorization = `Bearer ${localStorage.token}`;

  if (!!store.getState().area)
    config.headers['X-AreaId'] = store.getState().area.id;
  if (!!i18n.language) config.headers['X-Localization'] = i18n.language;

  return config;
});

api.interceptors.response.use(
  (response) => {
    if (response.config.headers['X-Show-Alert']) {
      const { headers } = response.config;
      const refresh = headers['X-Refresh'] ?? false;
      swal.success(
        headers['X-Success-Message'],
        headers['X-Redirect'],
        refresh
      );
    }
    return response;
  },
  (error) => {
    if (
      error.response.status === 401 &&
      error.response.data.message === 'Unauthenticated.'
    ) {
      localStorage.removeItem('token');
      document.location.reload();
    }

    if (
      error.response.status === 400 &&
      typeof error.response.data === 'object'
    ) {
      error.response.data.forEach((error) =>
        toast.error(i18n.t(`error:${error}`), { autoClose: false })
      );
    }

    if (
      error.response.status === 500 &&
      typeof error.response.data === 'object'
    ) {
      swal.error(error.response.data.message);
    }

    return Promise.reject(error);
  }
);

export { api };
