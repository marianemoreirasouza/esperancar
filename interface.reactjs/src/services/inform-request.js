import { api } from './index';

const InformRequestService = {
  get(id, params) {
    return api.get('informs/' + id, params);
  },
  create(params) {
    return api.post('informs/', params);
  },
  getAll(params) {
    return api.get('informs/', params);
  }
};

export default InformRequestService;