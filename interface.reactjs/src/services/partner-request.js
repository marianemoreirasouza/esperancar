import { api } from './index';

const PartnerRequestService = {
  get(id, params) {
    return api.get('partners/' + id, params);
  },
  create(params) {
    return api.post('partners/', params);
  },
  update(id, params) {
    return api.post('partners/' + id, params);
  },
  delete(id) {
    return api.delete('partners/' + id);
  },
  getAll(params) {
    return api.get('partners/', params);
  }
};

export default PartnerRequestService;