import { api } from './index';

const DonationRequestService = {
    get(id, params) {
        return api.get('donations/' + id, params);
    },
    create(params) {
        return api.post('donations/', params);
    },
    getAll(params) {
        return api.get('donations/', params);
    },
    getDonationsData(params) {
        return api.get('donations-data/', params);
    }
};

export default DonationRequestService;