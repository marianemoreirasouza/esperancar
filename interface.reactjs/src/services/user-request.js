import { api } from './index';

const UserRequestService = {
  get(id, params) {
    return api.get('users/' + id, params);
  },
  create(params) {
    return api.post('users/', params);
  },
  update(id, params) {
    return api.post('users/' + id, params);
  },
  delete(id) {
    return api.delete('users/' + id);
  },
  getAll(params) {
    return api.get('users/', params);
  },
  getAllWithProfile: async (params) => {
    return await api.get('users-with-profile', params);
  }
};

export default UserRequestService;