import { api } from './index';

const FAQRequestService = {
  create(params) {
    return api.post('faq/', params);
  },
  getAll(params) {
    return api.get('faq/', params);
  }
};

export default FAQRequestService;