import { api } from './index';

const ActionRequestService = {
  get(id, params) {
    return api.get('actions/' + id, params);
  },
  create(params) {
    return api.post('actions/', params);
  },
  getAll(params) {
    return api.get('actions/', params);
  },
  getAllWithUsers(params) {
    return api.get('actions-with-users/', params);
  },
};

export default ActionRequestService;