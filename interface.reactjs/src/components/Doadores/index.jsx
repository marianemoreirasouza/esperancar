import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import UserRequestService from 'services/user-request.js';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { maskDocument, maskPhone } from 'helpers/document';
import Input from 'helpers/Input';
import FormatDateBR from 'helpers/FormatDate';
import { formatDate } from 'helpers/date';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
import Dialog from 'react-bootstrap-dialog';

const DoadoresList = () => {

  const Data = [];
  const [doadores, setDoadores] = useState([]);
  const [show, setShow] = useState(false);
  const [deleted, setDeleted] = useState(false);
  const [user, setUser] = useState(false);
  const [edited, setEdited] = useState(false);
  const [submited, setSubmited] = useState(false);
  const [modal, setModal] = useState(false);
  const handleShow = () => setShow(true);
  const [isLoad, setLoad] = useState(false);
  const handleClose = () => {
    setUser(false);
    setShow(false);
  };

  const { SearchBar } = Search;
  const { ExportCSVButton } = CSVExport;
  const columns = [
    {
      dataField: 'id',
      text: '#'
    },
    {
      dataField: 'name',
      text: 'Nome'
    },
    {
      dataField: 'cpf',
      text: 'CPF'
    },
    {
      dataField: 'blood_type',
      text: 'Tipo Sanguíneo'
    },
    {
      dataField: 'sex',
      text: 'Sexo'
    },
    {
      dataField: 'edit',
      text: 'Ações'
    }];
  let dialog;
  const sizePerPageRenderer = ({
    options,
    onSizePerPageChange
  }) => (
    <span className="react-bs-table-sizePerPage-dropdown dropdown">
      <select className="dropdown-pagination col-3" onChange={(e) => {
        onSizePerPageChange(e.target.value);
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }}>
        {
          options.map((option) => {
            return (
              <option
                value={option.page}
              >
                {option.text}
              </option>
            );
          })
        }
      </select>
    </span>
  );
  const customTotal = (from, to, size) => (
    <span className="react-bootstrap-table-pagination-total col-9 float-right text-left">
      Mostrando { from} a { to} de { size} registros
    </span>
  );
  const options = {
    sizePerPageRenderer,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    sizePerPageList: [
      {
        text: '5', value: 5
      },
      {
        text: '25', value: 25
      },
      {
        text: '50', value: 50
      },
      {
        text: 'Todos', value: doadores.length,
      }
    ]
  };

  useEffect(() => {
    let filters = {
      profile_type: "Doador"
    };
    let request = UserRequestService.getAllWithProfile({ params: { ...filters } });
    request.then(data => {
      let users = data.data.data;
      setDoadores(users);
    })

    if (submited || edited) {
      let message = edited
        ? "Doador atualizado com sucesso!"
        : "Doador criado com sucesso!";
      setLoad(false);
      Alert.success(message).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          renderData();
          setSubmited(false);
          setEdited(false);
        }
      });
    }

    if (deleted) {
      let message = "Doador inativado com sucesso!"
      setLoad(false);
      Alert.success(message).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          renderData();
          setDeleted(false);
        }
      });
    }
  }, [edited, submited, deleted]);

  const deleteDoador = (doador) => {
    Dialog.setOptions({
      defaultOkLabel: 'Sim',
      defaultCancelLabel: 'Não',
      primaryClassName: 'btn-success',
      defaultButtonClassName: 'btn-danger',
    });

    dialog.show({
      title: "Inativar usuário?",
      body: 'Você têm certeza',
      actions: [Dialog.CancelAction(), Dialog.OKAction(() =>
        UserRequestService.delete(doador.id).then(data => {
          setDeleted(true);
        }))],
      bgSize: 'small',
    });
  }

  const btn = (doador) => {
    return (<>
      <Button type="button" className="btn-info waves-effect waves-light"
        onClick={() => {
          setUser(doador);
          setModal(true);
          handleShow();
        }}>
        <EditIcon />
      </Button>

      <Button type="button" className="btn-info waves-effect waves-light"
        onClick={() => {
          deleteDoador(doador);
        }}>
        <DeleteIcon />
      </Button>
    </>
    );
  };

  const renderData = () => {
    doadores.map((item, k) => {
      Data.push({
        id: item.id,
        name: item.name,
        cpf: maskDocument(item.cpf),
        blood_type: item.blood_type,
        sex: item.sex,
        edit: btn(item)
      });
    });
  }

  const setValue = (name, value) => {
    let user_aux = { ...user };
    user_aux[name] = value;
    setUser({ ...user_aux });
  };

  const submit = () => {
    setLoad(true);
    let model = [
      { key: 'id' },
      { key: 'name', required: true },
      { key: 'cpf', required: true },
      { key: 'main_phone', required: true },
      { key: 'msg_phone' },
      { key: 'sex', required: true },
      { key: 'blood_type', required: true },
      { key: 'birthdate', required: true },
      { key: 'address', required: true },
      { key: 'email', required: true },
      { key: 'password', required: true },
    ];
    let erro = {
      status: false,
      erros: []
    };

    const formData = new FormData();

    model.forEach((item) => {
      if (item.required) {
        if (user[item.key] && user[item.key] !== '' && user[item.key] !== null && user[item.key] !== 0)
          formData.append(item.key, user[item.key]);
        else {
          erro.status = true;
          erro.erros.push(item.key + ' required');
        }
      }
      else
        formData.append(item.key, user[item.key] ? user[item.key] : '');
    });

    if (user.id) {
      formData.append("_method", "put");
      if (!erro.status) {
        UserRequestService.update(user.id, formData).then(data => {
          setLoad(false);
          setEdited(true);
          handleClose();
        });
      } else {
        setLoad(false);
      }
    } else {
      if (!erro.status) {
        UserRequestService.create(formData)
          .then(data => {
            setLoad(false);
            setSubmited(true);
            handleClose();
          });
      } else {
        setLoad(false);
      }
    }
  }

  renderData();
  return (
    <>
      <Dialog
        ref={(component) => {
          dialog = component;
        }}
      />
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 class="font-weight-bold">Doadores</h4>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col>
            <Card.Body className="card-dashboard shadow bg-white rounded text-center h-100 w-100">
              <Row>
                <Col xs='12' className="text-left m-3">
                  <Button onClick={() => {
                    setModal(true);
                    handleShow();
                  }}>
                    Adicionar doador
                  </Button>
                </Col>
              </Row>
              <Row>
                <Col xs="12">
                  <ToolkitProvider
                    id="table"
                    keyField="id"
                    data={Data}
                    columns={columns}
                    search
                    exportCSV={{
                      fileName: 'Relatório - Doadores.csv',
                      separator: ';'
                    }}
                  >
                    {
                      props => (
                        <>
                          <div className='text-left'>
                            {/* <ExportCSVButton className="btn btn-primary" {...props.csvProps}>Exportar</ExportCSVButton> */}
                          </div>
                          <div className='text-right'>
                            <SearchBar {...props.searchProps} placeholder={'Buscar'} />
                          </div>
                          <BootstrapTable
                            bordered={false}
                            className="font-10"
                            hover
                            condensed
                            wrapperClasses={'table-responsive'}
                            {...props.baseProps}
                            pagination={paginationFactory(options)}
                          />

                        </>
                      )
                    }
                  </ToolkitProvider>
                </Col>
              </Row>
            </Card.Body>
          </Col>
        </Row>
        {modal ? (<>
          <Modal size="md"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Col xs='6'>
                <Modal.Title className="font-weight-bold font-16">
                  {user.id ? 'Editar doador' : 'Criar doador'}
                </Modal.Title>
              </Col>
            </Modal.Header>
            <Modal.Body>
              <Row className="mb-2">
                <Col xs="12" className="font-13 pr-3">
                  <Form.Group>
                    <Form.Label>Nome Completo:</Form.Label>
                    <Form.Control
                      as="input"
                      value={user ? user.name : ''}
                      onChange={e => setValue('name', e.target.value)}
                    ></Form.Control>
                    <Form.Label className="mt-3">CPF:</Form.Label>
                    <Input.CPF
                      value={user ? user.cpf : ''}
                      onChange={e => setValue('cpf', e.target.value)}
                    ></Input.CPF>
                    <Form.Label className="mt-3">Sexo:</Form.Label>
                    <Form.Control
                      as="select"
                      value={user ? user.sex : ''}
                      onChange={e => setValue('sex', e.target.value)}
                    >
                      <option value='' disabled={true}>Selecione</option>
                      <option value='F'>Feminino</option>
                      <option value='M'>Masculino</option>
                    </Form.Control>
                    <Form.Label className="mt-3">Tipo Sanguíneo:</Form.Label>
                    <Form.Control
                      as="select"
                      value={user ? user.blood_type : ''}
                      onChange={e => setValue('blood_type', e.target.value)}
                    >
                      <option value='' disabled={true}>Selecione</option>
                      <option value='A+'>A+</option>
                      <option value='A-'>A-</option>
                      <option value='B+'>B+</option>
                      <option value='B-'>B-</option>
                      <option value='AB+'>AB+</option>
                      <option value='AB-'>AB-</option>
                      <option value='O+'>O+</option>
                      <option value='O-'>O-</option>
                    </Form.Control>
                    <Form.Label className="mt-3">Telefone principal:</Form.Label>
                    <Input.Phone
                      value={user ? user.main_phone : ''}
                      onChange={e => setValue('main_phone', e.target.value)}
                    ></Input.Phone>
                    <Form.Label className="mt-3">Telefone para recados:</Form.Label>
                    <Input.Phone
                      value={user ? user.msg_phone : ''}
                      onChange={e => setValue('msg_phone', e.target.value)}
                    ></Input.Phone>
                    <Form.Label className="mt-3">Data de Nascimento:</Form.Label>
                    <DatePicker
                      selected={user && user.birthdate ? new Date(FormatDateBR.parseDate(user.birthdate)) : ''}
                      onChange={e => setValue('birthdate', formatDate(e))}
                      dateFormat="dd/MM/yyyy"
                      className='form-control'
                    />
                    <Form.Label className="mt-3">Endereço</Form.Label>
                    <Form.Control
                      as="input"
                      value={user ? user.address : ''}
                      onChange={e => setValue('address', e.target.value)}
                    ></Form.Control>
                    <Form.Label className="mt-3">E-mail:</Form.Label>
                    <Form.Control
                      as="input"
                      value={user ? user.email : ''}
                      onChange={e => setValue('email', e.target.value)}
                    ></Form.Control>
                    <Form.Label className="mt-3">Senha:</Form.Label>
                     <Form.Control
                      as="input"
                      value={user ? user.password : ''}
                      onChange={e => setValue('password', e.target.value)}
                    ></Form.Control>
                  </Form.Group>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Row className="col-12">
                {isLoad ? (
                  <Spinner animation="border" style={{ color: '#e05c17' }} />
                ) : (
                    <Button onClick={() => submit()}>Salvar</Button>
                  )}
              </Row>
            </Modal.Footer>
          </Modal>
        </>) : ''}
      </Container>
    </>
  );
};

export default MasterPage(DoadoresList);