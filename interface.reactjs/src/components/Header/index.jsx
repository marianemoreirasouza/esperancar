import React from 'react';
import { connect } from 'react-redux';
import { ASYNC_USER_LOGOUT } from 'store';
import { Link } from 'react-router-dom';
import { Nav, NavDropdown } from 'react-bootstrap';
import style from './style.module.scss';
import { useTranslation } from 'react-i18next';

const Header = ({
  user,
  enlarged,
  setEnlarged,
  className,
  logout
}) => {

  const { t } = useTranslation();

  return (
    <div className={`${style.header} ${className}`}>
      <ul className="list-unstyled float-right mb-0">
        <Nav className="mr-auto">
          <NavDropdown className="nav-text-white" title={user.name}>
            <NavDropdown.Item onClick={logout}>
              {t('title:logout')}
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </ul>

      <div className="logo-box">
        <Link to="/">
          <span className="logo-lg">
            <img
              src="/logo-name.png"
              alt="Esperançar"
              height={28}
            />
          </span>
          <span className="logo-sm">
            <img
              src="/logo-sm.png"
              alt="Esperançar"
              height={30}
            />
          </span>
        </Link>
      </div>
      <ul className="list-unstyled m-0">
        <li>
          <button className="bars" onClick={() => setEnlarged(!enlarged)}>
            <i className="fas fa-bars"></i>
          </button>
        </li>
      </ul>
    </div>
  );
};

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch({ type: ASYNC_USER_LOGOUT })
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
