import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import FAQRequestService from 'services/faq-request.js';
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
require('dotenv').config();

const FAQ = () => {

    const [submited, setSubmited] = useState(false);
    const [isLoad, setLoad] = useState(false);
    const [faqText, setFAQText] = useState([]);
    const [info, setInfo] = useState({});

    const submit = () => {
        setLoad(true);

        const formData = new FormData();
        formData.append('text', faqText);

        FAQRequestService.create(formData)
            .then(data => {
                setLoad(false);
                setSubmited(true);
            });

        setLoad(false);
    }


    useEffect(() => {
        let filters = {};
        let request = FAQRequestService.getAll({ params: { ...filters } });
        request.then(data => {
            let faqs = data.data.data[data.data.data.length - 1];
            setFAQText(faqs.text);
        });

        if (submited) {
            let message = "FAQ atualizado com sucesso!";
            setLoad(false);
            Alert.success(message).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    setSubmited(false);
                }
            });
        }
    }, [submited]);

    return (
        <>
            <Container>
                <Row className="col-12">
                    <Col md="12">
                        <h4 className="font-weight-bold">FAQ</h4>
                    </Col>
                </Row>
                <Row className="mt-3 mb-3">
                    <Col>
                        <Card.Body className="card-dashboard shadow bg-white rounded text-left h-100 w-100">
                            <Row>
                                <Col xs="12">
                                    <p className="mb-0">Digite abaixo o texto do FAQ.</p>
                                    <p className="mb-0">Para deixar o texto em <b>NEGRITO</b> no app, coloque seu texto entre as tags <b>{`<b>`}</b>. Exemplo: {`<b>`}Seu texto{`</b>`}.</p>
                                    <p className="mb-4">Para inserir um <b>LINK</b> para abrir no app, coloque a URL entre as tags <b>{`<a>`}</b>. Exemplo: {`<a>`}https://esperancar.com.br{`</a>`}.</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12">
                                    <Form.Group>
                                        <Form.Control
                                            as="textarea"
                                            rows='20'
                                            disabled={isLoad}
                                            value={faqText ?? ''}
                                            onChange={e => setFAQText(e.target.value)}
                                        >
                                        </Form.Control>
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Row className="col-12">
                                        {isLoad ? (
                                            <Spinner animation="border" style={{ color: '#e05c17' }} />
                                        ) : (
                                                <Button onClick={() => submit()}>Salvar</Button>
                                            )}
                                    </Row>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default MasterPage(FAQ);