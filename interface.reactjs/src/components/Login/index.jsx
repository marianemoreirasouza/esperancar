import React, { useState } from 'react';
import { connect } from 'react-redux';
import { ASYNC_USER_LOGIN } from 'store';
import { Link, Redirect } from 'react-router-dom';
import { Form, Button, Alert, Card, Row, Col, Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import './style.css';

import { api } from 'services';

const Login = ({ authenticate, login, location, error }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { from } = location.state || { from: { pathname: '/' } };
  const { t } = useTranslation();

  var emailF = '';

  function openPassword() {
    var form2 = document.getElementById('form-2');
    var form1 = document.getElementById('form-1');
    if (form2 != null && form1 != null) {
      form2.style.display = 'block';
      form1.style.display = 'none';
    }
  }

  function closePassword() {
    var form2 = document.getElementById('form-2');
    var form1 = document.getElementById('form-1');
    if (form2 != null && form1 != null) {
      form2.style.display = 'none';
      form1.style.display = 'block';
    }
  }

  if (authenticate) return <Redirect to={from.pathname} location={location} />;

  return (
    <>
      <div className='login' id="login">
        <Container className="mb-5">
          <Row className="justify-content-center mb-5">
            <Col md="4"></Col>
            <Col md="4" className="mt-5">
              <Card>
                <Card.Body>
                  <Row>
                    <Col className="text-center">
                      <img src="/logo.png" alt="Esperançar" className="esperancar-logo" />
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      {!!error && (
                        <Alert className="mt-4" variant="danger">
                          {error}
                        </Alert>
                      )}
                      <Form
                        id="form-1"
                        onSubmit={e => {
                          e.preventDefault();
                          login(email, password);
                        }}
                      >
                        <Form.Group>
                          <Form.Label>{t('label:email')}</Form.Label>
                          <Form.Control
                            type="email"
                            placeholder={t('label:type your email')}
                            onChange={e => setEmail(e.target.value)}
                            required
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Label>{t('label:password')}</Form.Label>
                          <Form.Control
                            type="password"
                            placeholder={t('label:type your password')}
                            onChange={e => setPassword(e.target.value)}
                            required
                          />
                        </Form.Group>
                        <Form.Group>
                          <Form.Check type="checkbox" label={t('label:remember me')} />
                        </Form.Group>
                        <Button block type="submit" className="login-btn">
                          Login
            </Button>
                        <p className="text-center mt-2">
                          <label className="forgot-password"
                            onClick={e => {
                              openPassword();
                            }}>
                            {t('label:forgot your password')}
                          </label>
                        </p>
                      </Form>
                      <Form
                        id="form-2"
                        onSubmit={e => {
                          e.preventDefault();
                          document.getElementById('forgot-btn').innerHTML = '<div class="loading">Enviando</div>';
                          document.getElementById('forgot-btn').disabled = true;
                          api.post('forgotten-password', { email: emailF }).then(({ data }) => {
                            if (data.status === 'not found') {
                              document.getElementById('forgot-btn').innerHTML = 'Enviar e-mail';
                              document.getElementById('forgot-btn').disabled = false;
                              document.getElementById('emailHelp').style.display = '';
                            }

                            if (data.status === 'ok') {
                              document.getElementById('forgot-btn').innerHTML = 'Enviar e-mail';
                              document.getElementById('forgot-btn').disabled = false;

                              closePassword();
                            }
                          });
                        }}
                      >
                        <p className="mt-2">
                          <Link to="/" className="forgot-password"
                            onClick={
                              closePassword()
                            }>
                            <i class="fas fa-chevron-left"></i> Retornar para o login
              </Link>
                        </p>
                        <Form.Group>

                          <Form.Label>{t('label:email')}</Form.Label>
                          <Form.Control
                            type="email"
                            placeholder={t('label:type your email')}
                            onChange={e => {
                              emailF = e.target.value;
                              document.getElementById('emailHelp').style.display = 'none';
                            }}
                            required
                          />
                          <Form.Text style={{ height: '10px' }}>
                            <span id="emailHelp" class="text-danger" style={{ display: 'none' }}>
                              E-mail não registrado
                </span >
                          </Form.Text>
                        </Form.Group>
                        <Button block type="submit" className="forgot-btn" id="forgot-btn">
                          Enviar e-mail
            </Button>
                      </Form>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </Col>
            <Col md="4"></Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  authenticate: state.user.authenticate,
  error: state.user.error
});

const mapDispatchToProps = dispatch => ({
  login: (email, password) =>
    dispatch({
      type: ASYNC_USER_LOGIN,
      payload: { email, password }
    })
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
