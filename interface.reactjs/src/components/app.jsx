import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import store, { ASYNC_USER_LOAD_DATA, history } from 'store';
import { ConnectedRouter } from 'connected-react-router';
import { connect } from 'react-redux';
import { api } from 'services';
import { PublicRoutes, PrivateRoutes } from './routes';
import { ToastContainer } from 'react-toastify';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import './style.scss';

import Loading from 'components/Loading';
import PrivateRoute from 'helpers/PrivateRoute';

const Routes = connect(
  (state) => ({
    isLogged: state.user.authenticate,
    pathname: state.router.location.pathname,
    isLoading: state.loading,
  }),
  (dispatch) => ({
  })
)(({ isLogged, pathname, isLoading }) => {
  useEffect(() => {
    
  }, []);

  if (!!localStorage.token && !isLogged) return <Loading />;

  return (
    <>
      <ToastContainer />

      {PublicRoutes.map((route, k) => (
        <Route
          key={k}
          exact={!!route.exact}
          path={route.pathname}
          component={route.component}
          {...route.props}
        />
      ))}

      {PrivateRoutes.map((route, k) => (
        <PrivateRoute
          key={k}
          exact={!!route.exact}
          path={route.pathname}
          component={route.component}
          {...route.props}
          permission={route.permission}
        />
      ))}
    </>
  );
});

const App = () => {
  useEffect(() => {
    if (!!localStorage.token) store.dispatch({ type: ASYNC_USER_LOAD_DATA });
  }, []);

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Switch>
          <Routes />
        </Switch>
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
