import React, { useState } from 'react';
import { connect } from 'react-redux';
import { ASYNC_USER_LOGIN } from 'store';
import { Link, Redirect } from 'react-router-dom';
import { Form, Button, Alert } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import './style.css';

import { api } from 'services';

const ResetPassword = ({
  authenticate,
  location,
  registerpassword,
  match: {
    params: { email,token }
  }, 
  }) => {
  const [status, setStatus] = useState('error');
  const { from } = location.state || { from: { pathname: '/login' } };
  const [reports, setReports] = useState({ data: [] });
  const { t } = useTranslation();

  var erro_password = {status:false,message:''};
  var password = '';
  var passwordconfirm = '';

  function teste(){
    api.post('reset-password/verify-token', { token:token, email:email } ).then(({ data }) => {
        setStatus(data.status);
    } );
  }
  teste();
  function submit_password(password){
    if(!erro_password.status){  
      api.post('reset-password', { token:token, email:email, password:password } ).then(({ data }) => {
        window.location.href = location;
      });
    }
  }
  function error(status, message, id){
    erro_password.status=status;
    erro_password.message = message;
    var erro = document.getElementById(id);
    if (erro!=null){
      erro.style.display=status?'':'none';
      erro.innerText=erro_password.message;
    }
  }
  

  if (authenticate) return <Redirect to={from.pathname} location={location} />;
  if (status=='not found'){
    return <Redirect to={from.pathname} location={location} />;
  } 

  function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
  }
  function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
  }

  return (
    <div className="login" id="registerpassword">
      <main className="login-page">
        <div className="login-form">
          <span>
            <img src="/logo-light.png" alt="ESPERANÇAR" className="scan-logo" />
          </span>
          <Form
            onSubmit={e => {
              e.preventDefault();
              submit_password(password);
            }}
          >
            <Form.Group>
              <Form.Label>{t('label:password')}</Form.Label>
              <Form.Control
                id='password'
                type="password"
                placeholder="Digite uma nova senha"
                onChange={e => {
                  if(isEmptyOrSpaces(e.target.value)) error(true, 'Digite uma senha correta',"passwordHelp1")
                  else if(hasWhiteSpace(e.target.value)) error(true, 'Senha não pode ter espaço',"passwordHelp1")
                  else if(document.getElementById("password2").value!=e.target.value) error(true, 'As senhas estão diferentes',"passwordHelp2")
                  else error(false,'','passwordHelp1');
                  password = e.target.value;
                }}
                required
              />
              <Form.Text style={{height:'10px'}}>
                <span  id="passwordHelp1" class="text-danger" style={{display:'none'}}>
                  Must be 8-20 characters long.
                </span >      
              </Form.Text>
            </Form.Group>
            <Form.Group>
              <Form.Label>Confirmar senha</Form.Label>
              <Form.Control
                id="password2"
                type="password"
                placeholder="Confirmer sua nova senha"
                onChange={e => {
                  if(isEmptyOrSpaces(e.target.value)) error(true, 'Digite uma senha correta',"passwordHelp2")
                  else if(hasWhiteSpace(e.target.value)) error(true, 'Senha não pode ter espaço',"passwordHelp2")
                  else if(document.getElementById("password").value!=e.target.value) error(true, 'As senhas estão diferentes',"passwordHelp2")
                  else error(false,'','passwordHelp2');
                  passwordconfirm = e.target.value;
                  
                }}
                required
              />
              <Form.Text style={{height:'10px'}}>
                <span  id="passwordHelp2" class="text-danger" style={{display:'none'}}>
                  Must be 8-20 characters long.
                </span >      
              </Form.Text>
            </Form.Group>
            <Button block type="submit" className="login-btn">
              Alterar Senha
            </Button>
          </Form>
        </div>
      </main>
    </div>
  );
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ResetPassword);