import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import DonationRequestService from 'services/donation-request.js';
import Chart from "react-apexcharts";
import { collect } from 'collect.js';

const PorTipo = () => {

    const Data = [];
    const { ExportCSVButton } = CSVExport;
    const [donations, setDonations] = useState([]);

    const bloods = ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'];
    const qtd = [];

    const columns = [
        {
            dataField: 'blood_type',
            text: 'Tipo Sanguíneo'
        },
        {
            dataField: 'qty_bags',
            text: 'Quantidade de bolsas'
        }

    ];

    const chart = {

        series: qtd,
        options: {
            chart: {
                width: '100%',
                type: 'pie',
            },
            colors: ["#EF233C", "#2B2D42", "#4ECDC4", "#8D99AE", "#1A535C", "#4EA8DE", "#EDF2F4", "#FFE66D"],
            labels: bloods,
            plotOptions: {
                pie: {
                    dataLabels: {
                        offset: -5
                    }
                }
            },
            legend: {
                show: true
            }
        },
    };

    const renderData = () => {
        bloods.map((blood, k) => {
            let qty = collect(donations).where('blood_type', blood).sum('qty_bags');
            qtd.push(qty);

            Data.push({
                blood_type: blood,
                qty_bags: qty
            });
        });
    }

    useEffect(() => {
        let filters = {
            embed: 'acao,doador'
        };
        let request = DonationRequestService.getAll({ params: { ...filters } });
        request.then(data => {
            let donations = data.data.data;
            setDonations(donations);
        });
        renderData();
    }, []);

    renderData();
    return (
        <>
            <Container>
                <Row className="col-12">
                    <Col md="12">
                        <h4 className="font-weight-bold">Relatório - Por tipo Sanguíneo</h4>
                    </Col>
                </Row>
                <Row className="mt-3 mb-3">
                    <Col md={6}>
                        <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                            <Row>
                                <Col xs="12">
                                    <ToolkitProvider
                                        id="table"
                                        keyField="id"
                                        data={Data}
                                        columns={columns}
                                        exportCSV={{
                                            fileName: 'Relatório - Por tipo sanguíneo.csv',
                                            separator: ';'
                                        }}
                                    >
                                        {
                                            props => (
                                                <>
                                                    <div>
                                                        <ExportCSVButton className="btn btn-primary mb-4" {...props.csvProps}>Exportar</ExportCSVButton>
                                                    </div>
                                                    <div className="text-center">
                                                        <BootstrapTable
                                                            bordered={false}
                                                            className="font-10"
                                                            hover
                                                            condensed
                                                            wrapperClasses={'table-responsive'}
                                                            {...props.baseProps}
                                                        />
                                                    </div>
                                                </>
                                            )
                                        }
                                    </ToolkitProvider>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Col>
                    <Col md={6}>
                        <Card className="no-border">
                            <Card.Header>
                                Quantidade de bolsas por tipo sanguíneo
                            </Card.Header>
                            <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                                <Chart
                                    options={chart.options}
                                    series={chart.series}
                                    type="pie"
                                />
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default MasterPage(PorTipo);