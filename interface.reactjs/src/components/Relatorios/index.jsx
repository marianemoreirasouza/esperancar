import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';

const RelatoriosList = () => {

  return (
    <>
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 className="font-weight-bold">Relatórios</h4>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Doações por mês
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-16">Relatório de doadores mês a mês</p>
                <Button className="float-right" href={"/relatorios/doacoes-mes"}>Acessar</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Por tipo sanguíneo
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-16">Relatório com quantidade por tipo sanguíneo</p>
                <Button className="float-right" href={"/relatorios/por-tipo-sanguineo"}>Acessar</Button>
              </Card.Body>
            </Card>
          </Col>

          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Doadores fieis
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-16">Relatório de doadores que costumam participar de ações</p>
                <Button className="float-right" href={"/relatorios/doadores-fieis"}>Acessar</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default MasterPage(RelatoriosList);