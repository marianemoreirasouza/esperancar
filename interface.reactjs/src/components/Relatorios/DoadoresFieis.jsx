import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import UserRequestService from 'services/user-request.js';
import { maskDocument } from 'helpers/document';
import { collect } from 'collect.js';

const DoadoresFieis = () => {

    const Data = [];
    const { SearchBar } = Search;
    const { ExportCSVButton } = CSVExport;
    const [doadores, setDoadores] = useState([]);

    const columns = [
        {
            dataField: 'id',
            text: '#'
        },
        {
            dataField: 'name',
            text: 'Nome'
        },
        {
            dataField: 'cpf',
            text: 'CPF'
        },
        {
            dataField: 'blood_type',
            text: 'Tipo Sanguíneo'
        },
        {
            dataField: 'sex',
            text: 'Sexo'
        },
        {
            dataField: 'qty_donations',
            text: 'Quantidade de doações'
        },
    ];

    const sizePerPageRenderer = ({
        options,
        onSizePerPageChange
    }) => (
        <span className="react-bs-table-sizePerPage-dropdown dropdown">
            <select className="dropdown-pagination col-3" onChange={(e) => {
                onSizePerPageChange(e.target.value);
                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }}>
                {
                    options.map((option) => {
                        return (
                            <option
                                value={option.page}
                            >
                                {option.text}
                            </option>
                        );
                    })
                }
            </select>
        </span>
    );
    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total col-9 float-right text-left">
            Mostrando { from} a { to} de { size} registros
        </span>
    );
    const options = {
        sizePerPageRenderer,
        showTotal: true,
        paginationTotalRenderer: customTotal,
        sizePerPageList: [
            {
                text: '5', value: 5
            },
            {
                text: '25', value: 25
            },
            {
                text: '50', value: 50
            },
            {
                text: 'Todos', value: doadores.length,
            }
        ]
    };

    const renderData = () => {
        let fieis = collect(doadores).where('qty_donations', '>', 1);
        fieis.map((item, k) => {
            Data.push({
                id: item.id,
                name: item.name,
                cpf: maskDocument(item.cpf),
                blood_type: item.blood_type,
                sex: item.sex,
                qty_donations: item.qty_donations
            });
        });
    }

    useEffect(() => {
        let filters = {
            profile_type: "Doador"
        };
        let request = UserRequestService.getAllWithProfile({ params: { ...filters } });
        request.then(data => {
            let users = data.data.data;
            setDoadores(users);
        })
        renderData();
    }, []);

    renderData();
    return (
        <>
            <Container>
                <Row className="col-12">
                    <Col md="12">
                        <h4 className="font-weight-bold">Relatório - Doações por Mês</h4>
                    </Col>
                </Row>
                <Row className="mt-3 mb-3">
                    <Col>
                        <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                            <Row>
                                <Col xs="12">
                                    <ToolkitProvider
                                        id="table"
                                        keyField="id"
                                        data={Data}
                                        columns={columns}
                                        search
                                        exportCSV={{
                                            fileName: 'Relatório - Doadores fieis.csv',
                                            separator: ';'
                                        }}
                                    >
                                        {
                                            props => (
                                                <>
                                                    <div>
                                                        <ExportCSVButton className="btn btn-primary" {...props.csvProps}>Exportar</ExportCSVButton>
                                                    </div>
                                                    <div className='text-right'>
                                                        <SearchBar {...props.searchProps} placeholder={'Buscar'} />
                                                    </div>
                                                    <BootstrapTable
                                                        bordered={false}
                                                        className="font-10"
                                                        hover
                                                        condensed
                                                        wrapperClasses={'table-responsive'}
                                                        {...props.baseProps}
                                                        pagination={paginationFactory(options)}
                                                    />

                                                </>
                                            )
                                        }
                                    </ToolkitProvider>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default MasterPage(DoadoresFieis);