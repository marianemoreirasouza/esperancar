import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import PartnerRequestService from 'services/partner-request.js';
import FileUpload from 'components/FileUpload';
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
import Dialog from 'react-bootstrap-dialog';
require('dotenv').config();

const ParceirosList = () => {

  const [modal, setModal] = useState(false);
  const [show, setShow] = useState(false);
  const [submited, setSubmited] = useState(false);
  const handleShow = () => setShow(true);
  const [isLoad, setLoad] = useState(false);
  const [partners, setPartners] = useState([]);
  const [partner, setPartner] = useState({});
  const [imgPreview, setImgPreview] = useState(null);
  const [imgPartner, setimgPartner] = useState(null);
  const [message, setMessage] = useState('');

  let dialog;

  const handleClose = () => {
    setPartner({});
    setImgPreview(null);
    setimgPartner(null);
    setShow(false);
  };

  const submit = (type, partnerId = null) => {
    setLoad(true);

    const formData = new FormData();

    formData.append('name', partner['name']);
    formData.append('url', partner['url']);

    if (imgPartner && imgPartner.length !== 0) formData.append('img_partner', imgPartner[0]);

    console.log(imgPartner)

    if (type == 'create') {
      PartnerRequestService.create(formData)
        .then(data => {
          setLoad(false);
          setMessage("criado");
          setSubmited(true);
          handleClose();
        });
    } else if (type == "update") {
      formData.append("_method", "put");
      PartnerRequestService.update(partnerId, formData)
        .then(data => {
          setLoad(false);
          setMessage("atualizado");
          setSubmited(true);
          handleClose();
        });
    } else if (type == "delete") {
      PartnerRequestService.delete(partnerId)
        .then(data => {
          setLoad(false);
          setMessage("excluído");
          setSubmited(true);
          handleClose();
        });
    }

    setLoad(false);
  }

  const deletePartner = (partnerId) => {
    Dialog.setOptions({
      defaultOkLabel: 'Sim',
      defaultCancelLabel: 'Não',
      primaryClassName: 'btn-success',
      defaultButtonClassName: 'btn-danger',
    });

    dialog.show({
      title: "Deseja excluir esse parceiro?",
      body: 'Você têm certeza',
      actions: [Dialog.CancelAction(), Dialog.OKAction(() =>
        submit('delete', partnerId))],
      bgSize: 'small',
    });
  }

  const setValue = (name, value) => {
    let partner_aux = { ...partner };
    partner_aux[name] = value;
    setPartner({ ...partner_aux });
  };

  const logo = (partner) => {
    return (<>
      <a className="logo-partner"
        onClick={() => {
          setPartner(partner);
          setModal(true);
          setImgPreview(`${process.env.REACT_APP_API_URL}/storage/${partner.url_img}`);
          handleShow();
        }}>
        <img className="logo-partner" width="75%" src={process.env.REACT_APP_API_URL + `/storage/` + partner.url_img}></img>
      </a>
    </>
    );
  };

  useEffect(() => {
    let filters = {};
    let request = PartnerRequestService.getAll({ params: { ...filters } });
    request.then(data => {
      let parts = data.data.data;
      setPartners(parts);
    });

    if (submited) {
      let text = `Parceiro ${message} com sucesso!`;
      setLoad(false);
      Alert.success(text).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          setSubmited(false);
        }
      });
    }
  }, [submited]);

  return (
    <>
      <Dialog
        ref={(component) => {
          dialog = component;
        }}
      />
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 className="font-weight-bold">Parceiros</h4>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col>
            <Card.Body className="card-dashboard shadow bg-white rounded text-center h-100 w-100">
              <Row>
                <Col xs='12' className="text-left m-3">
                  <Button onClick={() => {
                    setModal(true);
                    handleShow();
                  }}>
                    Novo Parceiro
                  </Button>
                </Col>
              </Row>
              <Row>
                {partners ? partners.map((part, k) => {
                  return (
                    <Col xs="4" key={k}>
                      <Card className="p-2 m-2">
                        <Row className="text-center">
                          <Col md="12">
                            <Row>
                              {logo(part)}
                            </Row>
                            <Row>
                              <Col md="12"><p className="mt-2 mb-0 font-weight-bold">{part.name}</p></Col>
                            </Row>
                            <Row>
                              <Col md="12"><a href={part.url}>{part.url}</a></Col>
                            </Row>
                          </Col>
                        </Row>
                      </Card>

                    </Col>
                  )
                })
                  : ''}
              </Row>
            </Card.Body>
          </Col>
        </Row>
        {modal ? (<>
          <Modal size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Col xs='6'>
                <Modal.Title className="font-weight-bold font-16">
                  {partner.id ? 'Parceiro' : 'Novo parceiro'}
                </Modal.Title>
              </Col>
            </Modal.Header>
            <Modal.Body>
              <Row className="mb-2">
                <Col md='6'>
                  <Form.Group>
                    <Form.Label className="mt-3">Nome</Form.Label>
                    <Form.Control
                      as="input"
                      value={partner.name ?? ''}
                      onChange={e => setValue('name', e.target.value)}
                    >
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col md='6'>
                  <Form.Group>
                    <Form.Label className="mt-3">Site</Form.Label>
                    <Form.Control
                      as="input"
                      value={partner.url ?? ''}
                      onChange={e => setValue('url', e.target.value)}
                    >
                    </Form.Control>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <Form.Group>
                    <Form.Label>Imagem</Form.Label>
                    <FileUpload
                      file={imgPartner}
                      setFile={setimgPartner}
                      preview={imgPreview}
                      setPreview={setImgPreview}
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Row className="col-12">
                {isLoad ? (
                  <Spinner animation="border" style={{ color: '#e05c17' }} />
                ) : (

                    !partner.id ?
                      <Button onClick={() => submit('create')}>Criar</Button>
                      :
                      <>
                        <Button className="m-1" onClick={() => submit('update', partner.id)}>Atualizar</Button>
                        <Button variant="danger" className="m-1" onClick={() => deletePartner(partner.id)}>Excluir</Button>
                      </>
                  )}
              </Row>
            </Modal.Footer>
          </Modal>
        </>) : ''}
      </Container>
    </>
  );
};

export default MasterPage(ParceirosList);