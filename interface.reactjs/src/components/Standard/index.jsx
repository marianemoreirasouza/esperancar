import React from 'react';
import { Dropzone } from 'react-dropzone-uploader/dist/styles.css';

const Standard = () => {
  const getUploadParams = () => {
    return { url: 'https://httpbin.org/post' };
  };

  const handleChangeStatus = ({ meta }, status) => {};

  const handleSubmit = (files, allFiles) => {
    files.map(f => f.meta);
    allFiles.forEach(f => f.remove());
  };

  return (
    <Dropzone
      getUploadParams={getUploadParams}
      onChangeStatus={handleChangeStatus}
      onSubmit={handleSubmit}
      styles={{ dropzone: { minHeight: 200, maxHeight: 250 } }}
    />
  );
};

export default Standard;
