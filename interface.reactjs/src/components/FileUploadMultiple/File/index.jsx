import React from 'react';
import style from './style.module.scss';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

const File = ({ extension, comment, uri }) => {
  return (
    <div className={style.icon}>
      <OverlayTrigger placement="top" overlay={<Tooltip>{comment}</Tooltip>}>
        <a tabIndex="-1" target="_blank" href={uri} rel="noopener noreferrer">
          <div className={`icon ${extension}`}></div>
        </a>
      </OverlayTrigger>
      {/* <div className='actions'>
                <i className='fas fa-trash'></i>
                <Link download to={name} ><i className='fas fa-file-download'></i></Link>
            </div> */}
    </div>
  );
};

export default File;
