import React, { useCallback } from 'react';
import style from './style.module.scss';
import { Row, Col, Table, Form, Button } from 'react-bootstrap';
import { useDropzone } from 'react-dropzone';
import { useTranslation } from 'react-i18next';
import File from './File';

const FileUploadMultiple = ({
  files,
  setFiles,
  filesComments,
  setFilesComments,
  existingFiles,
  disabled
}) => {
  const onDrop = useCallback(
    acceptedFiles => {
      const updateArray = [...files];
      const updateArrayComments = [...filesComments];

      acceptedFiles.forEach(file => {
        updateArray.push(file);
        updateArrayComments.push({ value: '' });
      });

      setFilesComments(updateArrayComments);
      setFiles(updateArray);
    },
    [files, setFiles, filesComments, setFilesComments]
  );

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({ onDrop });

  const { t } = useTranslation();

  const handleChangeFileComment = (key, value) => {
    const updateArray = [...filesComments];
    updateArray[key].value = value;
    setFilesComments(updateArray);
  };

  const handleDeleteFile = key => {
    const updateArray = [...files];
    const updateArrayComments = [...filesComments];

    updateArray.splice(key, 1);
    updateArrayComments.splice(key, 1);

    setFiles(updateArray);
    setFilesComments(updateArrayComments);
  };

  return (
    <>
      <Row>
        <Col>
          <div
            {...getRootProps({
              className:
                style.dropzone +
                (isDragActive ? ' active' : '') +
                (isDragAccept ? ' accept' : '') +
                (isDragReject ? ' reject' : '')
            })}
          >
            <input {...getInputProps()} disabled={disabled} />
            {!isDragActive ? (
              <span>{t('label:drag here')}</span>
            ) : isDragAccept ? (
              <span>{t('label:drop now')}</span>
            ) : (
              <span>{t('label:file not accepted')}</span>
            )}
          </div>
        </Col>
      </Row>
      <Row
        className="mt-2"
        style={{ display: files.length === 0 ? 'none' : null }}
      >
        <Col>
          <Table bordered hover responsive>
            <thead>
              <tr>
                <th className="text-center align-middle">{t('label:file')}</th>
                <th className="text-center align-middle">
                  {t('label:comment')}
                </th>
                <th width={1} className="text-center align-middle"></th>
              </tr>
            </thead>
            <tbody>
              {files.map((file, k) => (
                <tr key={k}>
                  <td className="text-center align-middle">{file.name}</td>
                  <td className="text-center align-middle">
                    <Form.Control
                      value={filesComments[k].value}
                      onChange={e => handleChangeFileComment(k, e.target.value)}
                    />
                  </td>
                  <td className="text-center align-middle">
                    <Button
                      size="sm"
                      variant="danger"
                      onClick={() => handleDeleteFile(k)}
                    >
                      <i className="fas fa-times"></i>
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
      <Row className="mt-4">
        <Col>
          {existingFiles.map((file, k) => (
            <File
              key={k}
              extension={file.extension}
              comment={file.comment}
              uri={file.uri}
            />
          ))}
        </Col>
      </Row>
    </>
  );
};

export { File };
export default FileUploadMultiple;
