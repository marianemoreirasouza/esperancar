import React from 'react';
import { BounceLoader, BeatLoader } from 'react-spinners';
import style from './style.module.scss';
import './style.css';

const Loading = ({ page }) => {

  if (page) {
    return (
      <div className={style.loadingPage}>
        <BeatLoader size={20} color={'#F57B23'} />
      </div>
    );
  }

  return (
    <div className={style.loading}>
      <BounceLoader sizeUnit={'px'} size={120} color={'#F57B23'} />
    </div> 
  );
};

export default Loading;
