import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Accordion } from 'react-bootstrap';
import style from './style.module.scss';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PieChartIcon from '@material-ui/icons/PieChart';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import OpacityIcon from '@material-ui/icons/Opacity';
import GroupIcon from '@material-ui/icons/Group';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';

const Menu = ({ className, menu }) => {

  const createMenu = (className = '') => (
    <Accordion>
      <ul className={`list-unstyled ${className}`}>
        <li>
          <Link to={'/'}>
            <DashboardIcon className="icon-menu" />
            <span className="menu-item">{'Início'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/doadores'}>
            <GroupIcon className="icon-menu" />
            <span className="menu-item">{'Doadores'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/acoes'}>
            <OpacityIcon className="icon-menu" />
            <span className="menu-item">{'Ações'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/informes'}>
            <AnnouncementIcon className="icon-menu" />
            <span className="menu-item">{'Informes'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/relatorios'}>
            <PieChartIcon className="icon-menu" />
            <span className="menu-item">{'Relatórios'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/faq'}>
            <HelpOutlineIcon className="icon-menu" />
            <span className="menu-item">{'FAQ'}</span>
          </Link>
        </li>
        <li>
          <Link to={'/parceiros'}>
            <BusinessCenterIcon className="icon-menu" />
            <span className="menu-item">{'Parceiros'}</span>
          </Link>
        </li>
      </ul>
    </Accordion>
  );

  return <div className={`${style.menu} ${className}`}>{createMenu(menu)}</div>;
};

const mapStateToProps = state => ({
  menu: state.menu
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
