import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card } from 'react-bootstrap';
import Chart from "react-apexcharts";
import MasterPage from 'components/MasterPage';
import DonationRequestService from 'services/donation-request.js';
import ActionRequestService from 'services/action-request.js';
import collect from 'collect.js';
import {parseMonthYearBR} from 'helpers/date';

const Dashboard = () => {

  const [donationsChartBags, setDonationsChartBags] = useState({ date: [], qty: [] });
  const [donationsChartQty, setDonationsChartQty] = useState({ date: [], qty: [] });
  const [donations, setDonations] = useState([]);
  const [lastAction, setLastAction] = useState({id: '', date: ''});

  useEffect(() => {
    
    let request = ActionRequestService.getAll({ params: {} });
    request.then(data => {
      let actions = data.data.data;
      setLastAction(actions[actions.length-1]);
    });

    let filters = { type: 'bags' };
    request = DonationRequestService.getDonationsData({ params: { ...filters } });
    request.then(data => {
      let donations = data.data;
      setDonationsChartBags(donations);
    });

    filters = { type: 'qtd' };
    request = DonationRequestService.getDonationsData({ params: { ...filters } });
    request.then(data => {
      let donations = data.data;
      setDonationsChartQty(donations);
    });

    filters = { embed: 'acao' };
    request = DonationRequestService.getAll({ params: { ...filters } });
    request.then(data => {
      let donations = data.data.data;
      setDonations(donations);
    });

  }, []);

  const chartBags = {
    options: {
      chart: {
        id: "chart-bags"
      },
      xaxis: {
        categories: donationsChartBags.date
      },
      stroke: {
        show: true,
        curve: 'smooth',
        colors: "#A2041C"
      },
      markers: {
        size: 5,
        colors: "#4EA8DE"
      }
    },
    series: [
      {
        name: "Quantidade aproximada de bolsas de sangue",
        data: donationsChartBags.qty
      }
    ]
  };


  const chartQty = {
    options: {
      chart: {
        id: "chart-qty"
      },
      xaxis: {
        categories: donationsChartQty.date
      },
      colors: ["#A2041C"]
    },
    series: [
      {
        name: "Quantidade de doações",
        data: donationsChartQty.qty
      },
    ]
  };

  return (
    <>
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 className="font-weight-bold">Início</h4>
          </Col>
        </Row>
        <Row className="col-12 mt-4">
          <Col md="12">
            <h5 className="font-weight-bold">Ação atual - <b>{parseMonthYearBR(lastAction.date)}</b></h5>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Total de doações realizadas
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-weight-bold font-24">{donations ? donations.length : 0} doações</p>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Total de bolsas de sangue doadas
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-weight-bold font-24">{collect(donations).sum('qty_bags')} bolsas</p>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Qtd. de doações feitas na ação atual
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-weight-bold font-24">{lastAction ? collect(donations).where('action_id', lastAction.id).count(): 0} doações</p>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            <Card className="no-border">
              <Card.Header>
                Qtd. de bolsas doadas na ação atual
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <p className="font-weight-bold font-24">{lastAction ? collect(donations).where('action_id', lastAction.id).sum('qty_bags') : 0} bolsas</p>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col md={6}>
            <Card className="no-border">
              <Card.Header>
                Qtd. doações x Ação
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <Chart
                  options={chartQty.options}
                  series={chartQty.series}
                  type="bar"
                />
              </Card.Body>
            </Card>
          </Col>
          <Col md={6}>
            <Card className="no-border">
              <Card.Header>
                Qtd. bolsas de sangue x Ação
            </Card.Header>
              <Card.Body className="card-dashboard shadow bg-white rounded h-100 w-100">
                <Chart
                  options={chartBags.options}
                  series={chartBags.series}
                  type="line"
                />
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default MasterPage(Dashboard);