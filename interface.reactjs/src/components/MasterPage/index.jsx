import React from 'react';
import { connect } from 'react-redux';
import style from './style.module.scss';
import Header from 'components/Header';
import Menu from 'components/Menu';

const MasterPage = WrappedComponent => {
  return connect(
    state => ({
      enlarged: state.menu_enlarged
    }),
    dispatch => ({
      setEnlarged: payload => dispatch({ type: 'MENU_ENLARGED', payload })
    })
  )(props => {
    const classEnlarged = props.enlarged ? ' enlarged' : '';

    return (
      <div className={style.layout}>
        <Header
          className={classEnlarged}
          enlarged={props.enlarged}
          setEnlarged={props.setEnlarged}
        />
        <Menu className={classEnlarged} />
        <div className={style.content_page + classEnlarged}>
          <WrappedComponent {...props} />
        </div>
      </div>
    );
  });
};

export default MasterPage;
