import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import ActionRequestService from 'services/action-request.js';
import UserRequestService from 'services/user-request.js';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { parseDateMonthBR, parseDateYearBR } from 'helpers/date';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
import { maskDocument } from 'helpers/document';
import { toast } from 'react-toastify';
import ReactTooltip from 'react-tooltip';

const AcoesList = () => {

  const [actions, setActions] = useState([]);
  const [doadores, setDoadores] = useState([]);
  const [usersAction, setUsersActions] = useState([]);
  const [show, setShow] = useState(false);
  const [submited, setSubmited] = useState(false);
  const [modal, setModal] = useState(false);
  const handleShow = () => setShow(true);
  const [isLoad, setLoad] = useState(false);
  const handleClose = () => {
    setUsersActions([]);
    setShow(false);
  };
  const Data = [];
  const { SearchBar } = Search;
  const DataDoadores = [];
  const columns = [
    {
      dataField: 'id',
      text: '#'
    },
    {
      dataField: 'month',
      text: 'Mês'
    },
    {
      dataField: 'year',
      text: 'Ano'
    },
    {
      dataField: 'show',
      text: 'Ações'
    }];
  const sizePerPageRenderer = ({
    options,
    onSizePerPageChange
  }) => (
    <span className="react-bs-table-sizePerPage-dropdown dropdown">
      <select className="dropdown-pagination col-3" onChange={(e) => {
        onSizePerPageChange(e.target.value);
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }}>
        {
          options.map((option) => {
            return (
              <option
                value={option.page}
              >
                {option.text}
              </option>
            );
          })
        }
      </select>
    </span>
  );
  const customTotal = (from, to, size) => (
    <span className="react-bootstrap-table-pagination-total col-9 float-right text-left">
      Mostrando { from} a { to} de { size} registros
    </span>
  );
  const optionsAcoes = {
    sizePerPageRenderer,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    sizePerPageList: [
      {
        text: '5', value: 5
      },
      {
        text: '25', value: 25
      },
      {
        text: '50', value: 50
      },
      {
        text: 'Todos', value: actions.length,
      }
    ]
  };

  const optionsDoadores = {
    sizePerPageRenderer,
    showTotal: true,
    paginationTotalRenderer: customTotal,
    sizePerPageList: [
      {
        text: '5', value: 5
      },
      {
        text: '10', value: 10
      }
    ]
  };

  const selectAll = () => {
    return (
      <>
        <p className="mb-0 mt-4">Selecionar</p>
        <Form.Group>
          <Form.Check
            id="checkDoador"
            type="checkbox"
            onChange={(e) => {
              setUsersActions([]);
              if (e.target.checked) {
                let aux = [];
                doadores.map((item, k) => {
                  aux.push(item.id);
                });
                setUsersActions(aux);
              }
            }
            }
          />
        </Form.Group>
      </>
    )
  }
  const columnsDoadores = [
    {
      dataField: 'id',
      text: '#',
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle',
        width: '5%'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle',
        width: '5%'
      },
    },
    {
      dataField: 'name',
      text: 'Nome',
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
    },
    {
      dataField: 'cpf',
      text: 'CPF',
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
    },
    {
      dataField: 'blood_type',
      text: 'Tipo Sanguíneo',
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
    },
    {
      dataField: 'sex',
      text: 'Sexo',
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
    },
    {
      dataField: 'select',
      text: selectAll(),
      headerStyle: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
      style: {
        textAlign: 'center',
        verticalAlign: 'middle'
      },
    }];

  const insertDoador = (id, checked) => {
    let updateArray = [...usersAction];

    if (checked) {
      updateArray.push(id)
    } else {
      const index = updateArray.indexOf(id);
      if (index > -1) {
        updateArray.splice(index, 1);
      };
    }
    setUsersActions(updateArray);
  }

  const blocked = () => {
    let today = new Date();
    let dateToday = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0');
    let last = actions.length > 0 ? actions[actions.length - 1].date : '';

    if (last.slice(0, -3) == dateToday)
      return true;

    return false;
  };

  const btn = (id) => {
    return (<>
      <Button type="button" href={`acoes/show/${id}`} className="btn-info waves-effect waves-light">
        <VisibilityIcon />
      </Button>
    </>
    );
  };

  const check = (id) => {
    return (
      <>
        <Form.Group>
          <Form.Check
            id="checkDoador"
            type="checkbox"
            checked={usersAction.indexOf(id) > -1}
            onChange={(e) => insertDoador(id, e.target.checked)}
          />
        </Form.Group>
      </>
    )
  }

  const getMonth = (month) => {
    switch (month) {
      case '01':
        return 'Janeiro';
      case '02':
        return 'Fevereiro';
      case '03':
        return 'Março';
      case '04':
        return 'Abril';
      case '05':
        return 'Maio';
      case '06':
        return 'Junho';
      case '07':
        return 'Julho';
      case '08':
        return 'Agosto';
      case '09':
        return 'Setembro';
      case '10':
        return 'Outubro';
      case '11':
        return 'Novembro';
      case '12':
        return 'Dezembro';
    }
  }

  const renderData = () => {
    actions.map((item, k) => {
      Data.push({
        id: item.id,
        month: getMonth(parseDateMonthBR(item.date)),
        year: parseDateYearBR(item.date),
        show: btn(item.id)
      });
    });
  }

  const canDonate = (user) => {

    let date1 = new Date();
    let date2 = new Date(user.last_donation)
    let diffDays = Math.ceil(Math.abs(date2 - date1) / (1000 * 60 * 60 * 24)) - 1

    console.log(user, diffDays)
    if (user.sex == 'F')
      return diffDays >= 90 ? true : false;
    else
      return diffDays >= 60 ? true : false;
  }

  const renderDataDoadores = () => {
    doadores.map((item) => {
      console.log(canDonate(item))
      if (canDonate(item)) {
        DataDoadores.push({
          id: item.id,
          name: item.name,
          cpf: maskDocument(item.cpf),
          blood_type: item.blood_type,
          sex: item.sex,
          select: check(item.id)
        });
      }
    });
  }

  const submit = () => {
    setLoad(true);

    let date = new Date();
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0');
    let yyyy = date.getFullYear();
    date = yyyy + '-' + mm + '-' + dd;

    const formData = new FormData();

    formData.append('date', date);
    formData.append('doadores', usersAction);

    if (usersAction.length > 0) {
      ActionRequestService.create(formData)
        .then(data => {
          setLoad(false);
          setSubmited(true);
          handleClose();
        });
    } else {
      toast.error('É necessário selecionar ao menos um doador!');
      setLoad(false);
    }

    setLoad(false);
  }

  useEffect(() => {
    let filters = {};
    let request = ActionRequestService.getAll({ params: { ...filters } });
    request.then(data => {
      let actions = data.data.data;
      setActions(actions);
    });

    filters = {
      profile_type: "Doador"
    };
    request = UserRequestService.getAllWithProfile({ params: { ...filters } });
    request.then(data => {
      let users = data.data.data;
      setDoadores(users);
    });

    if (submited) {
      let message = "Ação criada com sucesso!";
      setLoad(false);
      Alert.success(message).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          renderData();
          setSubmited(false);
        }
      });
    }
  }, [submited]);

  renderData();
  renderDataDoadores();
  return (
    <>
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 className="font-weight-bold">Ações</h4>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col>
            <Card.Body className="card-dashboard shadow bg-white rounded text-center h-100 w-100">
              <Row>
                <Col xs='2' data-tip="Já existe uma ação em andamento para esse mês" className="text-left m-3">
                  <Button disabled={blocked()} onClick={() => {
                    setModal(true);
                    handleShow();
                  }}>
                    Nova ação
                  </Button>
                  {blocked() && <ReactTooltip />}
                </Col>
              </Row>
              <Row>
                <Col xs="12">
                  <ToolkitProvider
                    id="table"
                    keyField="id"
                    data={Data}
                    columns={columns}
                    search
                  >
                    {
                      props => (
                        <>
                          <div className='text-right'>
                            <SearchBar {...props.searchProps} placeholder={'Buscar'} />
                          </div>
                          <BootstrapTable
                            bordered={false}
                            className="font-10"
                            hover
                            condensed
                            wrapperClasses={'table-responsive'}
                            {...props.baseProps}
                            pagination={paginationFactory(optionsAcoes)}
                          />

                        </>
                      )
                    }
                  </ToolkitProvider>
                </Col>
              </Row>
            </Card.Body>
          </Col>
        </Row>
        {modal ? (<>
          <Modal size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Col xs='6'>
                <Modal.Title className="font-weight-bold font-16">
                  Criar ação
                </Modal.Title>
              </Col>
            </Modal.Header>
            <Modal.Body>
              <Row className="mb-2">
                <Col xs="12">
                  <ToolkitProvider
                    id="table"
                    keyField="id"
                    data={DataDoadores}
                    columns={columnsDoadores}
                    search
                  >
                    {
                      props => (
                        <>
                          <SearchBar {...props.searchProps} placeholder={'Buscar'} />
                          <BootstrapTable
                            bordered={false}
                            className="font-10"
                            hover
                            condensed
                            wrapperClasses={'table-responsive'}
                            {...props.baseProps}
                            pagination={paginationFactory(optionsDoadores)}
                          />

                        </>
                      )
                    }
                  </ToolkitProvider>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Row className="col-12">
                {isLoad ? (
                  <Spinner animation="border" style={{ color: '#e05c17' }} />
                ) : (
                    <Button onClick={() => submit()}>Enviar convite</Button>
                  )}
              </Row>
            </Modal.Footer>
          </Modal>
        </>) : ''}
      </Container>
    </>
  );
};

export default MasterPage(AcoesList);