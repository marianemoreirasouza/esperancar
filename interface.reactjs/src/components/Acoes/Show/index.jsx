import React, { useEffect, useState } from 'react';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import ActionRequestService from 'services/action-request.js';
import UserRequestService from 'services/user-request.js';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import { parseDateMonthBR, parseDateYearBR } from 'helpers/date';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
import Dialog from 'react-bootstrap-dialog';
import { maskDocument } from 'helpers/document';
import { toast } from 'react-toastify';
import { setMonth } from 'date-fns/esm';
import collect from 'collect.js';

const AcoesShow = ({
    match: {
        params: { id },
    }
}) => {

    const [month, setMonth] = useState('');
    const [year, setYear] = useState('');
    const [doadores, setDoadores] = useState([]);
    const [usersAction, setUsersActions] = useState([]);
    const [show, setShow] = useState(false);
    const [submited, setSubmited] = useState(false);
    const [modal, setModal] = useState(false);
    const handleShow = () => setShow(true);
    const [isLoad, setLoad] = useState(false);
    const handleClose = () => {
        setUsersActions([]);
        setShow(false);
    };
    const Data = [];
    const columns = [
        {
            dataField: 'id',
            text: '#',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle',
                width: '5%'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle',
                width: '5%'
            },
        },
        {
            dataField: 'name',
            text: 'Nome',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
        },
        {
            dataField: 'cpf',
            text: 'CPF',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
        },
        {
            dataField: 'blood_type',
            text: 'Tipo Sanguíneo',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
        },
        {
            dataField: 'sex',
            text: 'Sexo',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
        },
        {
            dataField: 'donation',
            text: 'Bolsas de Sangue',
            headerStyle: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
            style: {
                textAlign: 'center',
                verticalAlign: 'middle'
            },
        }];

    const getMonth = (month) => {
        switch (month) {
            case '01':
                return 'Janeiro';
            case '02':
                return 'Fevereiro';
            case '03':
                return 'Março';
            case '04':
                return 'Abril';
            case '05':
                return 'Maio';
            case '06':
                return 'Junho';
            case '07':
                return 'Julho';
            case '08':
                return 'Agosto';
            case '09':
                return 'Setembro';
            case '10':
                return 'Outubro';
            case '11':
                return 'Novembro';
            case '12':
                return 'Dezembro';
        }
    }

    const renderData = () => {
        doadores.map((item) => {
            Data.push({
                id: item.doador.id,
                name: item.doador.name,
                cpf: maskDocument(item.doador.cpf),
                blood_type: item.doador.blood_type,
                sex: item.doador.sex,
                donation: item.qty_bags
            });
        });
    }

    useEffect(() => {
        let filters = { id: id };
        let request = ActionRequestService.getAllWithUsers({ params: { ...filters } });
        request.then(data => {
            setDoadores(data.data.doacao);
            setMonth(getMonth(parseDateMonthBR(data.data.date)));
            setYear(parseDateYearBR(data.data.date));
        });
    }, []);

    renderData();
    return (
        <>
            <Container>
                <Row className="col-12">
                    <Col md="12">
                        <h4 className="font-weight-bold">Ação - {month + '/' + year}</h4>
                    </Col>
                </Row>
                <Row className="mt-3 mb-3">
                    <Col>
                        <Card.Body className="card-dashboard shadow bg-white rounded text-center h-100 w-100">
                            <Row>
                                <Col xs='12' className="text-left m-3">
                                    <h5>
                                        <span className="font-weight-bold">Quantidade aproximada de bolsas:</span> {collect(doadores).sum('qty_bags')}
                                    </h5>
                                </Col>
                            </Row>
                            <Col xs="12">
                                <BootstrapTable
                                    id="table"
                                    className="font-10"
                                    keyField="id"
                                    data={Data}
                                    columns={columns}
                                    bordered={false}
                                    hover
                                    condensed
                                />
                            </Col>
                        </Card.Body>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default MasterPage(AcoesShow);