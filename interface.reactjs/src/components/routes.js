import Login from 'components/Login';
import RegisterPassword from 'components/RegisterPassword';
import ResetPassword from 'components/ResetPassword';
import Dashboard from 'components/Dashboard';
import DoadoresList from 'components/Doadores/index';
import AcoesList from 'components/Acoes/List/index';
import AcoesShow from 'components/Acoes/Show/index';
import InformesList from 'components/Informes/index';
import Relatorios from 'components/Relatorios/index';
import DoacoesMes from 'components/Relatorios/DoacoesMes';
import PorTipo from 'components/Relatorios/PorTipo';
import DoadoresFieis from 'components/Relatorios/DoadoresFieis';
import FAQ from 'components/FAQ';
import Parceiros from 'components/Parceiros';

const PublicRoutes = [
  { pathname: '/login', component: Login, exact: true },
  {
    pathname: '/RegisterPassword/:token/:id',
    component: RegisterPassword,
    exact: true,
  },
  {
    pathname: '/ResetPassword/:token/:email',
    component: ResetPassword,
    exact: true,
  },
];

const PrivateRoutes = [
  { pathname: '/', component: Dashboard, exact: true },
  { pathname: '/doadores', component: DoadoresList, exact: true },
  { pathname: '/acoes', component: AcoesList, exact: true },
  { pathname: '/acoes/show/:id', component: AcoesShow, exact: true },
  { pathname: '/informes', component: InformesList, exact: true },
  { pathname: '/relatorios', component: Relatorios, exact: true },
  { pathname: '/relatorios/doacoes-mes', component: DoacoesMes, exact: true },
  { pathname: '/relatorios/por-tipo-sanguineo', component: PorTipo, exact: true },
  { pathname: '/relatorios/doadores-fieis', component: DoadoresFieis, exact: true },
  { pathname: '/faq', component: FAQ, exact: true },
  { pathname: '/parceiros', component: Parceiros, exact: true },
];

export { PublicRoutes, PrivateRoutes };
