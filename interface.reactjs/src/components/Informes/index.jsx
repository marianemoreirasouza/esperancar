import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Container, Col, Row, Card, Button, Form, Modal, Spinner } from 'react-bootstrap';
import MasterPage from 'components/MasterPage';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import InformRequestService from 'services/inform-request.js';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import { toast } from 'react-toastify';
import FileUpload from 'components/FileUpload';
import { parseDateBR } from 'helpers/date';
import Swal from 'sweetalert2';
import Alert from 'helpers/alert';
require('dotenv').config();

const InformesList = () => {

  const [modal, setModal] = useState(false);
  const [show, setShow] = useState(false);
  const [submited, setSubmited] = useState(false);
  const handleShow = () => setShow(true);
  const [isLoad, setLoad] = useState(false);
  const [informs, setInforms] = useState([]);
  const [info, setInfo] = useState({});
  const [imgPreview, setImgPreview] = useState(null);
  const [imgInfo, setImgInfo] = useState(null)
  const handleClose = () => {
    setInfo({});
    setImgPreview(null);
    setImgInfo(null);
    setShow(false);
  };

  const submit = () => {
    setLoad(true);

    const formData = new FormData();

    formData.append('type', info['type']);
    formData.append('text', info['text']);

    if (imgInfo && imgInfo.length !== 0) formData.append('img_info', imgInfo[0]);

    InformRequestService.create(formData)
      .then(data => {
        setLoad(false);
        setSubmited(true);
        handleClose();
      });

    setLoad(false);
  }

  const setValue = (name, value) => {
    let info_aux = { ...info };
    info_aux[name] = value;
    setInfo({ ...info_aux });
  };

  const renderIcon = (type) => {
    switch (type) {
      case 'info':
        return <InfoIcon className="icon-inform info" />;
      case 'important':
        return <WarningIcon className="icon-inform important" />
      case 'thank':
        return <EmojiEmotionsIcon className="icon-inform thank" />
      default:
        return <InfoIcon className="icon-inform" />
    }
  }

  const btn = (inform) => {
    return (<>
      <Button type="button" className="btn-info waves-effect waves-light"
        onClick={() => {
          setInfo(inform);
          setModal(true);
          setImgPreview(`${process.env.REACT_APP_API_URL}/storage/${inform.url_img}`);
          handleShow();
        }}>
        <VisibilityIcon />
      </Button>
      {/* 
      <Button type="button" className="btn-info waves-effect waves-light"
        onClick={() => {
          deleteInform(inform);
        }}>
        <DeleteIcon />
      </Button> */}
    </>
    );
  };

  useEffect(() => {
    let filters = {};
    let request = InformRequestService.getAll({ params: { ...filters } });
    request.then(data => {
      let infos = data.data.data;
      setInforms(infos);
    });

    if (submited) {
      let message = "Informe criado com sucesso!";
      setLoad(false);
      Alert.success(message).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
          // renderData();
          setSubmited(false);
        }
      });
    }
  }, [submited]);

  return (
    <>
      <Container>
        <Row className="col-12">
          <Col md="12">
            <h4 className="font-weight-bold">Informes</h4>
          </Col>
        </Row>
        <Row className="mt-3 mb-3">
          <Col>
            <Card.Body className="card-dashboard shadow bg-white rounded text-center h-100 w-100">
              <Row>
                <Col xs='12' className="text-left m-3">
                  <Button onClick={() => {
                    setModal(true);
                    handleShow();
                  }}>
                    Novo Informe
                  </Button>
                </Col>
              </Row>
              <Col xs="12">
                {informs ? informs.map((inform, k) => {
                  return (
                    <Card className="p-2 m-2">
                      <Row className="text-left">
                        <Col md="2" className="text-center mt-2">{renderIcon(inform.type)}</Col>
                        <Col md="8"><p className="mt-2">{inform.text}</p></Col>
                        <Col md="1" className="mt-2">{btn(inform)}</Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <span className="font-weight-bold">{parseDateBR(inform.created_at)}</span>
                        </Col>
                      </Row>
                    </Card>
                  )
                })

                  : ''}
              </Col>
            </Card.Body>
          </Col>
        </Row>
        {modal ? (<>
          <Modal size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Col xs='6'>
                <Modal.Title className="font-weight-bold font-16">
                  {info.id ? 'Informe' : 'Criar Informe'}
                </Modal.Title>
              </Col>
            </Modal.Header>
            <Modal.Body>
              <Row className="mb-2">
                <Col md='4'>
                  <Form.Group>
                    <Form.Label className="mt-3">Tipo</Form.Label>
                    <Form.Control
                      as="select"
                      disabled={info.id ? true : false}
                      value={info.type ?? ''}
                      onChange={e => setValue('type', e.target.value)}
                    >
                      <option value='' selected disabled>Selecione</option>
                      <option value='info' >Informação</option>
                      <option value='thank'>Agradecimento</option>
                      <option value='important'>Importante</option>
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col md='1' />
                <Col md='7'>
                  <Form.Group>
                    <Form.Label className="mt-3">Mensagem</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows='5'
                      disabled={info.id ? true : false}
                      value={info.text ?? ''}
                      onChange={e => setValue('text', e.target.value)}
                    >
                    </Form.Control>
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col md={12}>
                  <Form.Group>
                    <Form.Label>Imagem</Form.Label>
                    <FileUpload
                      file={imgInfo}
                      setFile={setImgInfo}
                      preview={imgPreview}
                      setPreview={setImgPreview}
                      disabled={info.id ? true : false}
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Modal.Body>
            {!info.id ?
              <Modal.Footer>
                <Row className="col-12">
                  {isLoad ? (
                    <Spinner animation="border" style={{ color: '#e05c17' }} />
                  ) : (
                      <Button onClick={() => submit()}>Criar</Button>
                    )}
                </Row>
              </Modal.Footer>
              : ''}
          </Modal>
        </>) : ''}
      </Container>
    </>
  );
};

export default MasterPage(InformesList);