import React, { useState } from 'react';
import Dropzone from 'react-dropzone';
import style from './style.module.scss';
import { Row, Col, Modal } from 'react-bootstrap';

const FileUpload = ({ file, setFile, preview, setPreview, disabled }) => {
  const [previewModal, setPreviewModal] = useState(false);

  return (
    <>
      <Row>
        <Col>
          <Dropzone
            accept="image/jpeg, image/png"
            multiple={false}
            disabled={disabled}
            onDrop={acceptedFiles => {
              setFile(acceptedFiles);
              setPreview(
                acceptedFiles.length !== 0
                  ? URL.createObjectURL(acceptedFiles[0])
                  : null
              );
            }}
          >
            {({
              getRootProps,
              getInputProps,
              isDragActive,
              isDragAccept,
              isDragReject
            }) => (
                <section>
                  <div
                    {...getRootProps({
                      className:
                        style.dropzone +
                        (isDragActive ? ' active' : '') +
                        (isDragAccept ? ' accept' : '') +
                        (isDragReject ? ' reject' : '')
                    })}
                  >
                    <input {...getInputProps()} />
                    {!disabled ? 
                    !isDragActive ? (
                      <span>Arraste até aqui</span>
                    ) : isDragAccept ? (
                      <span>Solte agora</span>
                    ) : (
                          <span>Arquivo não aceito</span>
                        )
                    : <span>Pré-visualização de imagem</span>}
                  </div>
                </section>
              )}
          </Dropzone>
        </Col>
        <Col md={2}>
          <div className={style.img_preview}>
            {!preview ? (
              <span>Prévia</span>
            ) : (
                <>
                  <img
                    className="thumb"
                    alt='Prévia'
                    src={preview}
                    onClick={() => setPreviewModal(true)}
                  />

                  <Modal
                    size="md"
                    show={previewModal}
                    onHide={() => setPreviewModal(false)}
                  >
                    <Modal.Header closeButton>
                      Prévia
                  </Modal.Header>
                    <Modal.Body>
                      <img
                        alt='Prévia'
                        width="100%"
                        heigth="100%"
                        src={preview}
                      />
                    </Modal.Body>
                  </Modal>
                </>
              )}
          </div>
        </Col>
      </Row>
    </>
  );
};

export default FileUpload;
