import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = (props) => {
  const { isLogged, component: Component, permission } = props;
  return (
    <Route
      {...props}
      component={(moreProps) =>
        isLogged ? (
          <Component {...props} {...moreProps} />
        ) : (
            <Redirect to="/login" />
          )
      }
    />
  );
};

export default connect((state) => ({
  isLogged: state.user.authenticate,
}))(PrivateRoute);
