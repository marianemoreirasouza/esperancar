import { conformToMask } from 'react-text-mask';

const documentType = document => {
    document += '';
    if (document.length < 12)
        return "CPF";
    else
        return "CNPJ";
}

const maskDocument = document => {
    document += '';
    var mask = [];
    // if (document.length > 14) {
    //     mask = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
    // } else {
    mask = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
    // }
    return conformToMask(
        document,
        mask, { guide: false }
    ).conformedValue;

}

const maskPhone = phone => {
    phone += '';
    var mask = [];
    
    if(phone.length > 14)
        mask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    else
        mask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

    return conformToMask(
        phone,
        mask, { guide: false }
    ).conformedValue;

}

const maskZip = zip => {
    zip += '';

    var mask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

    return conformToMask(
        zip,
        mask, { guide: false }
    ).conformedValue;

}

export { documentType, maskDocument, maskZip, maskPhone }