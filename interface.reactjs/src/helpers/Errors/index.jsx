import React from 'react';
import { Form } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

const Errors = ({ errors }) => {
  const { t } = useTranslation();

  if (!errors) return <></>;

  return (
    <Form.Control.Feedback type="invalid">
      {errors.map((error, k) => (
        <p key={k}>{t(`error:${error}`)}</p>
      ))}
    </Form.Control.Feedback>
  );
};

export default Errors;
