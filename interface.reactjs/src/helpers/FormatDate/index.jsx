const format_date = () => {
    //let platform = navigator.platform;
    //throw navigator.platform;
    const parseDate = date => {
      const split = date.split(' ')[0].split('-');
    
      var d = new Date([split[1], split[2], split[0]].join('/'));
      var month = '' + (d.getMonth() + 1);
      var day = '' + d.getDate();
    
      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;
    
      return [month, day, d.getFullYear()].join('/');
    };
  
    return {
      parseDate
    };
  };
  
  const FormatDateBR = format_date();
  
  export default FormatDateBR;