import React from 'react';
import DatePicker from 'react-datepicker';
import style from './style.module.scss';
import { Form } from 'react-bootstrap';
import 'react-datepicker/dist/react-datepicker.css';
import { useTranslation } from 'react-i18next';

const DayMonthPicker = props => {
  const { t } = useTranslation();

  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  return (
    <div className={style.daymonthpicker}>
      <DatePicker
        renderCustomHeader={({ date, changeMonth }) => {
          return (
            <div
              style={{
                margin: 10,
                display: 'flex',
                justifyContent: 'center'
              }}
            >
              <Form.Control
                as="select"
                value={date.getMonth()}
                onChange={e => changeMonth(e.target.value)}
              >
                {months.map((option, k) => (
                  <option key={k} value={k}>
                    {t(`label:${option}`)}
                  </option>
                ))}
              </Form.Control>
            </div>
          );
        }}
        openToDate={new Date('01/01/1004')}
        minDate={new Date('01/01/1004')}
        maxDate={new Date('12/31/1004')}
        customInput={<Form.Control />}
        dateFormat="dd/MM"
        placeholderText="dd/mm"
        {...props}
      />
    </div>
  );
};

export default DayMonthPicker;
