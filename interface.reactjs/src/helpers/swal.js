import Swal from 'sweetalert2';
import { history } from '../store'
const swal = {
  success: (text, redirect, refresh = false) => {
    Swal.fire({
      title: 'Sucesso!',
      text,
      icon: 'success',
      timer: 3000,
      showConfirmButton: false,
      timerProgressBar: true,
      allowOutsideClick: false
    })
    .then((result) => {
      if (result.dismiss === Swal.DismissReason.timer && redirect) {
        if (refresh) {
          history.push('/refresh')
        }
        history.push(redirect)
      }
    })
  },
  error: (text) => {
    Swal.fire({
      title: 'Ops!',
      html: text,
      icon: 'error',
      confirmButtonText: 'Tentar Novamente'
    })
  }
}

export default swal;
