import React from 'react';
import { Form } from 'react-bootstrap';
import MaskedInput from 'react-text-mask';

const CPF = props => (
  <MaskedInput
    mask={[
      /\d/,
      /\d/,
      /\d/,
      '.',
      /\d/,
      /\d/,
      /\d/,
      '.',
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/
    ]}
    pattern={'\\d{3}[.]\\d{3}[.]\\d{3}[-]\\d{2}'}
    className="form-control"
    {...props}
  />
);

const Phone = props => (
  <MaskedInput
    mask={
      props.cell
        ? [
            '(',
            /[1-9]/,
            /\d/,
            ')',
            ' ',
            /\d/,
            /\d/,
            /\d/,
            /\d/,
            /\d/,
            '-',
            /\d/,
            /\d/,
            /\d/,
            /\d/
          ]
        : [
            '(',
            /[1-9]/,
            /\d/,
            ')',
            ' ',
            /\d/,
            /\d/,
            /\d/,
            /\d/,
            '-',
            /\d/,
            /\d/,
            /\d/,
            /\d/,
            /\d/
          ]
    }
    pattern={'[(][1-9]\\d[)] \\d{4,5}[-]\\d{4}'}
    className="form-control"
    {...props}
  />
);

const CEP = props => (
  <MaskedInput
    mask={[
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
    ]}
    pattern={'\\d{5}[-]\\d{3}'}
    className="form-control"
    {...props}
  />
);

const stateRegistration = props => (
  <MaskedInput
    mask={[
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
    ]}
    pattern={'\\d{5}[-]\\d{3}'}
    className="form-control"
    {...props}
  />
);

const PlainText = props => (
  <Form.Control plaintext readOnly tabIndex="-1" {...props} />
);

export default { CPF, Phone, CEP, stateRegistration, PlainText };