const formatDate = date => {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};

const dbDateToPtBR = date => {
  const split = date.split(' ')[0].split('-');

  var d = new Date([split[1], split[2], split[0]].join('/'));
  var month = '' + (d.getMonth() + 1);
  var day = '' + d.getDate();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month].join('/');
};

const parseDateBR =( date = {})=> {
  const split = date.split(' ')[0].split('-');

  var d = new Date([split[1], split[2], split[0]].join('/'));
  var month = '' + (d.getMonth() + 1);
  var day = '' + d.getDate();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [day, month, d.getFullYear()].join('/');
};

const parseDate = date => {
  const split = date.split(' ')[0].split('-');

  var d = new Date([split[1], split[2], split[0]].join('/'));
  var month = '' + (d.getMonth() + 1);
  var day = '' + d.getDate();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [month, day, d.getFullYear()].join('/');
};

const curday = sp => {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();
  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;
  return yyyy + sp + mm + sp + dd + ' 00:00:00';
};

const parseDateMonthBR =( date = {})=> {
  const split = date.split(' ')[0].split('-');

  var d = new Date([split[1], split[2], split[0]].join('/'));
  var month = '' + (d.getMonth() + 1);

  if (month.length < 2) month = '0' + month;

  return month;
};

const parseDateYearBR =( date = {})=> {
  const split = date.split(' ')[0].split('-');

  var d = new Date([split[1], split[2], split[0]].join('/'));

  return d.getFullYear();
};

const parseMonthYearBR = (date = {}) => {
  const split = date.split(' ')[0].split('-');

  var month = '' + (split[1]);
  var year = '' + (split[0]);

  if (month.length < 2) month = '0' + month;

  switch (month) {
      case '01':
          return 'Janeiro/' + year;
      case '02':
          return 'Fevereiro/' + year;
      case '03':
          return 'Março/' + year;
      case '04':
          return 'Abril/' + year;
      case '05':
          return 'Maio/' + year;
      case '06':
          return 'Junho/' + year;
      case '07':
          return 'Julho/' + year;
      case '08':
          return 'Agosto/' + year;
      case '09':
          return 'Setembro/' + year;
      case '10':
          return 'Outubro/' + year;
      case '11':
          return 'Novembro/' + year;
      case '12':
          return 'Dezembro/' + year;
  }
};

export { formatDate, dbDateToPtBR, parseDateBR, parseDate, curday, parseDateMonthBR, parseDateYearBR, parseMonthYearBR };
