const flatten = (data, flatKey = '', response = {}) => {
  for (var [key, value] of Object.entries(data)) {
    const newFlatKey = `${flatKey}${key}`;
    if (
      typeof value === 'object' &&
      value !== null &&
      Object.keys(value).length > 0
    )
      flatten(value, `${newFlatKey}.`, response);
    else response[newFlatKey] = value;
  }

  return response;
};

export default flatten;
