import React from 'react';
import { Form } from 'react-bootstrap';
import Errors from 'helpers/Errors';

const FormControl = props => {
  return (
    <>
      <Form.Control isInvalid={!!props.validation} {...props}>
        {props.children}
      </Form.Control>
      <Errors errors={props.validation} />
    </>
  );
};

export default FormControl;
