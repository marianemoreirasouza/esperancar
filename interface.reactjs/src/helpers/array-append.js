const arrayAppend = (formData, key, data) => {
  if (data instanceof File) return formData.append(key, data);

  if (data === Object(data) || Array.isArray(data)) {
    for (var i in data) {
      arrayAppend(formData, key + '[' + i + ']', data[i]);
    }
  } else {
    formData.append(key, data);
  }
};

export default arrayAppend;
