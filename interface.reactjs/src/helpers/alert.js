import Swal from 'sweetalert2';
import { history } from '../store'

const alert = {
  success: (message) => {
    return Swal.fire({
      title: 'Sucesso!',
      text: message,
      icon: 'success',
      timer: 3000,
      showConfirmButton: false,
      timerProgressBar: true,
      allowOutsideClick: false
    })
  },
  error: (message) => {
    return Swal.fire({
      title: 'Ops!',
      html: message,
      icon: 'error',
      confirmButtonText: 'Tentar Novamente'
    })
  }
}

export default alert;