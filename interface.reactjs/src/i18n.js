import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import pt_BR from 'locales/pt_BR';

const resources = {
  pt_BR
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'pt_BR',
  keySeparator: false,
  interpolation: {
    escapeValue: false
  }
});

export default i18n;
