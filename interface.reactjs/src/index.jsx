import React from 'react';
import ReactDOM from 'react-dom';
import 'i18n';
import 'bluebird/js/browser/bluebird';
import HttpsRedirect from 'react-https-redirect';

import App from 'components/app';

// ReactDOM.render(<HttpsRedirect><App /></HttpsRedirect>, document.getElementById('app'));

ReactDOM.render(<App />, document.getElementById('app'));
