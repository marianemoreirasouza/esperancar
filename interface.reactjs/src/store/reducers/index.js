import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import user from './user';
import loading from './loading';
import menu_enlarged from './menu_enlarged';

export default history =>
  combineReducers({
    router: connectRouter(history),
    user,
    loading,
    menu_enlarged
  });
