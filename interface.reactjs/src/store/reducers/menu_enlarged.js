import { initialState } from 'store';

export default (state = initialState.menu_enlarged, { type, payload }) => {
  switch (type) {
    case 'MENU_ENLARGED':
      return payload;
    default:
      return state;
  }
};
