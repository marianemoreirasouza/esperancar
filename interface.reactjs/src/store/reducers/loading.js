import { initialState, LOADING } from 'store';

export default (state = initialState.loading, { type, payload }) => {
  switch (type) {
    case LOADING:
      return payload;
    default:
      return state;
  }
};
