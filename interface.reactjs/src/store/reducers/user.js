import {
  initialState,
  USER_AUTH,
  USER_UNAUTH,
  AUTH_ERROR,
  USER_LOAD_DATA
} from 'store';

export default (state = initialState.user, { type, payload }) => {
  switch (type) {
    case USER_LOAD_DATA:
      return { ...state, ...payload, authenticate: true };
    case USER_AUTH:
      return { ...state, error: '', authenticate: true };
    case USER_UNAUTH:
      return initialState.user;
    case AUTH_ERROR:
      return { ...state, error: payload };
    default:
      return state;
  }
};
