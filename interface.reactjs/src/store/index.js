import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';

import reducers from './reducers';
import rootSaga from './sagas';
import initialState from './initialState';

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const createStoreWithMiddleware = applyMiddleware(
  ...[sagaMiddleware, routerMiddleware(history)]
)(createStore);

export default createStoreWithMiddleware(
  reducers(history),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

sagaMiddleware.run(rootSaga);

export { initialState };
export * from './actions';
