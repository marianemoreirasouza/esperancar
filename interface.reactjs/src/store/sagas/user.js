import { all, call, put, takeLatest } from 'redux-saga/effects';
import { api } from 'services';
import {
  LOADING,
  USER_LOAD_DATA,
  ASYNC_USER_LOAD_DATA,
  ASYNC_CREATE_USER,
  ASYNC_EDIT_USER,
  ASYNC_DELETE_USER,
  ASYNC_RESTORE_USER
} from 'store';

function* userLoadData() {
  const response = yield call(api.get, 'auth/user');

  yield put({ type: USER_LOAD_DATA, payload: response.data });
}

function* createUser({ payload }) {
  yield put({ type: LOADING, payload: true });

  try {
    yield call(api.post, 'users', payload, {
      headers: {
        'X-Success-Message': 'Usuário cadastrado com sucesso',
        'X-Redirect': '/users',
        'X-Show-Alert': true
      }
    });
  } catch (error) {
  } finally {
    yield put({ type: LOADING, payload: false });
  }
}

function* editUser({ payload }) {
  yield put({ type: LOADING, payload: true });

  try {
    yield call(api.put, `users/${payload.id}`, payload, {
      headers: { 
        'X-Success-Message': 'Usuário atualizado com sucesso', 
        'X-Redirect': '/users',
        'X-Show-Alert': true
      }
    });
  } catch (error) {
  } finally {
    yield put({ type: LOADING, payload: false });
  }
}

function* deleteUser({ payload: { id } }) {
  yield put({ type: LOADING, payload: true });

  try {
    yield call(api.delete, `users/${id}`, {
      headers: { 
        'X-Success-Message': 'Usuário desativado com sucesso', 
        'X-Redirect': '/users',
        'X-Refresh': true,
        'X-Show-Alert': true
      }
    });
  } catch (error) {
  } finally {
    yield put({ type: LOADING, payload: false });
  }
}

function* restoreUser({ payload: { id } }) {
  yield put({ type: LOADING, payload: true });

  try {
    yield call(api.get, `users/restore/${id}`, {
      headers: { 
        'X-Success-Message': 'Usuário reativado com sucesso', 
        'X-Redirect': '/users',
        'X-Refresh': true,
        'X-Show-Alert': true
      }
    });
  } catch (error) {
  } finally {
    yield put({ type: LOADING, payload: false });
  }
}

export default function* root() {
  yield all([
    takeLatest(ASYNC_USER_LOAD_DATA, userLoadData),
    takeLatest(ASYNC_CREATE_USER, createUser),
    takeLatest(ASYNC_EDIT_USER, editUser),
    takeLatest(ASYNC_DELETE_USER, deleteUser),
    takeLatest(ASYNC_RESTORE_USER, restoreUser)
  ]);
}
