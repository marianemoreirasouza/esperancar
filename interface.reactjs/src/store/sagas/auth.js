import { all, call, put, takeLatest } from 'redux-saga/effects';
import { api } from 'services';
import {
  ASYNC_USER_LOGIN,
  ASYNC_USER_LOGOUT,
  ASYNC_USER_LOAD_DATA,
  AUTH_ERROR,
  USER_UNAUTH,
  USER_AUTH
} from 'store';

function* userLogin(action) {
  const {
    payload: { email, password }
  } = action;

  try {
    const response = yield call(api.post, 'auth/login', { email, password });
    const data = yield response.data;

    yield localStorage.setItem('token', data.access_token);

    yield put({ type: USER_AUTH });
    yield put({ type: ASYNC_USER_LOAD_DATA });
  } catch (error) {
    yield put({
      type: AUTH_ERROR,
      payload: 'Credenciais inválidas!',
      resp: error.response
    });
  }
}

function* userLogout() {
  yield call(api.get, 'auth/logout');
  yield localStorage.removeItem('token');
  yield put({ type: USER_UNAUTH });
}

export default function* root() {
  yield all([
    takeLatest(ASYNC_USER_LOGIN, userLogin),
    takeLatest(ASYNC_USER_LOGOUT, userLogout)
  ]);
}
