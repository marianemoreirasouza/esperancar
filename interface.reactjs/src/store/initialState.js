export default {
  user: {
    authenticate: false
  },
  area: null,
  loading: false,
  menu: [],
  menu_enlarged: false,
  profile: null
};
