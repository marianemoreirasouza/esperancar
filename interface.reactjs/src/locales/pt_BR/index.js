import title from './title';
import label from './label';
import error from './error.json';

export default {
  title,
  label,
  error,
};
